# FlashCard

FlashCard is one of the main contents of this app.

## Usage
When FlashCard tab is selected `FlashCardLevelTableViewController` first shows up, where the user can select level: Beginner, Intermediate, Advance, and Super Advance.
Ater selected the level, the app navigates you to `FlashCardLevelTableViewController` which is a table view controller and the user can choose a unit(a group of Kanji). After selected the unit, then the next table view, `VocabularyList` , shows up. This is also table view controller. Each cell is composed of Kanji, yomi, yomi in alphabet, and English meaning. When selected a cell, the user can see the Kanji data in a unit as PageView. Eacg page has an audio icon, twitter icon, and googleImage icon so that the user can hear the pronounciation of the Kanji, searching the way the Kanji is used, and the image of the Kanji.


## FlashCardLevelTableViewController
## FlashCardLevelTableViewController
## VocabularyList
## VovabularyTableViewCell
## FlashCardViewController
FlashCardViewController is a page container class which takes care of page content and model controller.

## FlashCardContentViewController
## FlashCardController
## FlashCardContentView
## FlashCardView


# UI





