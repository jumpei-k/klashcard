//
//  KStartViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 4/16/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
//
enum QuizLevel: Int {
    case Beginner = 1
    case Intermediate
    case Advance
    case Nerd
}

class KStartViewController: UIViewController {
    var startView: KStartView?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        // Load data source you need here
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.title = "Quiz"
        tabBarItem = UITabBarItem(tabBarSystemItem: .Favorites, tag: 1)
        println("\(NSStringFromClass(self.dynamicType)): initilized")
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    

    /// Lyfe cycle
    
    override func loadView() {
        println("\(NSStringFromClass(self.dynamicType)): loadView is called")
        super.loadView()
        
        self.view.backgroundColor = UIColor.lightGrayColor()
        startView = KStartView(frame: CGRectZero)
        self.view.addSubview(startView!)
        // layout subviews
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        println("\(NSStringFromClass(self.dynamicType)): viewWillAppear is called")
    }
    
    override func viewDidLayoutSubviews() {
        println("\(NSStringFromClass(self.dynamicType)): viewDidLayoutSubviews is called")

        // Layout code should be here
        super.viewDidLayoutSubviews()
        startView?.frame = CGRectInset(self.view.bounds, 10.0, 20.0);
        
    }
    
    override func viewDidLoad() {
        println("\(NSStringFromClass(self.dynamicType)): viewDidLoad is called")

        // Additional action to view
        // Add target etc..
        super.viewDidLoad()
        startView?.beginnerButton?.addTarget(self, action: ("ToQuiz:"), forControlEvents: UIControlEvents.TouchUpInside)
        startView?.intermediateButton?.addTarget(self, action: ("ToQuiz:"), forControlEvents:UIControlEvents.TouchUpInside)
        startView?.advancedButton?.addTarget(self, action: ("ToQuiz:"), forControlEvents: UIControlEvents.TouchUpInside)
        startView?.nerdButton?.addTarget(self, action: ("ToQuiz:"), forControlEvents: UIControlEvents.TouchUpInside)
       
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    func ToQuiz(sender: AnyObject) {
        let senderButton = sender as! UIButton
        let level: QuizLevel
        
        let userLevel: Int = senderButton.tag
        switch userLevel {
        case 1:
            level = .Beginner
        case 2:
            level = .Intermediate
        case 3:
            level = .Advance
        case 4:
            level = .Nerd
        default:
            level = .Beginner
        }
//        println("id is ")
//        println(senderButton.tag)
        let kquizViewCtl = KViewController(level: level)
        self.presentViewController(kquizViewCtl, animated: true, completion: nil)
//        println("toQuiz")
    }
    
    
    
}