
//
//  FlashCardContentViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/6/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import AVFoundation

protocol KlashCardDataPresentable {
    func setupKlashCard(kanji: Kanji)
}

class FlashCardContentViewController: UIViewController {

    @IBOutlet weak var flashCardView: UIView!
    var flashCardHeadsView: FlashCardHeadsView!
    var flashCardTailsView: FlashCardTailsView!
    var speechSynthesizer = AVSpeechSynthesizer()
    var twitterSearchButton: UIButton?
    var googleSearchButton: UIButton?
    var flashCardData: Kanji!

    @IBOutlet weak var playSoundButton: UIButton!

    var showingKlashCard: Bool = true

    // ------------------------------
    // - MARK: - View lifecycle
    // ------------------------------
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("tabbar.hidden = \(tabBarController?.tabBar.hidden)")

    }

    override func viewWillDisappear(animated: Bool) {
        tabBarController?.hideTabBar()
    }

    override func viewDidDisappear(animated: Bool) {
        tabBarController?.hideTabBar()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(NSStringFromClass(self.dynamicType)): viewDidLoad is called")
        flashCardHeadsView = FlashCardHeadsView.loadFromNib()
        flashCardTailsView = FlashCardTailsView.loadFromNib()
        flashCardTailsView.frame = flashCardView.bounds
        flashCardHeadsView.frame = flashCardView.bounds
        flashCardView.addSubview(flashCardHeadsView)
        setupKlashCard(flashCardData)
    }
    
    @IBAction func playSound(sender: AnyObject) {
        play()
    }
    
    func play() {
        let utter = AVSpeechUtterance(string:flashCardData.kanji)
        let v = AVSpeechSynthesisVoice(language: "ja-JP")
        utter.voice = v
        var rate = AVSpeechUtteranceMaximumSpeechRate - AVSpeechUtteranceMinimumSpeechRate
        rate = rate * 0.15 + AVSpeechUtteranceMinimumSpeechRate
        utter.rate = rate
        speechSynthesizer.delegate = self
        speechSynthesizer.speakUtterance(utter)
    }
    
    @IBAction func tapTwitterButton(sender: AnyObject) {
        goTwitter()
    }
    
    @IBAction func tapGoogleImageButton(sender: AnyObject) {
        goGoogleImage()
    }
    
    func goGoogleImage() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gglimgvc = storyboard.instantiateViewControllerWithIdentifier("GoogleImageView") as! GoogleImageViewController
        gglimgvc.keyword = flashCardData.kanji
        navigationController?.pushViewController(gglimgvc, animated: true)
    }

    func goTwitter() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let twwtvc = storyboard.instantiateViewControllerWithIdentifier("TwitterFeedViewController") as! TwitterFeedViewController
        twwtvc.query = flashCardData.kanji
        twwtvc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(twwtvc, animated: true)
    }

    
    @IBAction func tapGesture(sender: AnyObject) {
        flipCard()
    }


    func flipCard() {
        if(showingKlashCard) {
            UIView.transitionFromView(flashCardHeadsView, toView: flashCardTailsView, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromRight, completion: nil)
            showingKlashCard = false
        } else {
            UIView.transitionFromView(flashCardTailsView, toView: flashCardHeadsView!, duration: 0.3, options: UIViewAnimationOptions.TransitionFlipFromLeft, completion: nil)
            showingKlashCard = true
        }
    }
}

// ----------------------------------
// - MARK: - KlashCardDataPresentable
// ----------------------------------

extension FlashCardContentViewController: KlashCardDataPresentable {
    func setupKlashCard(kanji: Kanji) {
        flashCardHeadsView.kanjiLabel.text = kanji.kanji
        flashCardTailsView.hiraganaLabel.text = kanji.yomi
        flashCardTailsView.englishLabel.text = kanji.english
    }
}

// -------------------------------------
// - MARK: - AVSpeechSynthesizerDelegate
// -------------------------------------
extension FlashCardContentViewController: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didStartSpeechUtterance utterance: AVSpeechUtterance) {
        print("starting")
    }

    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance) {
        print("finished")
    }

    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let s = (utterance.speechString as NSString).substringWithRange(characterRange)
        print("about to say \(s)")
    }

}
