//
//  TwitterFeedViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/20/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import TwitterKit

enum AwfulError: ErrorType {
    case RequestError
    case ConnectionError
    case ClietError
}

let statusesShowEndpoint = "https://api.twitter.com/1.1/search/tweets.json"

class TwitterFeedViewController: UITableViewController, TWTRTweetViewDelegate {
    
    let NumberOfRequestedTweets = "60"
    var query: String? {
        didSet {
            tweets.removeAll()
            tableView.reloadData()
            params = ["q": query!, "count": NumberOfRequestedTweets, "result_type": "popular"]
            refresh()
        }
    }

    var params: [NSObject: AnyObject]!
    var clientError : NSError?
    let tweetTableReuseIdentifier = "TweetCell"
    var tweets = [TWTRTweet]()

    @IBAction func refresh(sender: UIRefreshControl?) {
//        Twitter.sharedInstance().
        
        
        // FIXME: In TwitterKit 2.0, there's no member `logInGuestWithCompletion`
        // Find a way to send API Request with no using `logInGuestWithCompletion`
//        Twitter.sharedInstance().logInGuestWithCompletion { (session, error) in
//            guard session != nil else {
//                print(error)
//                return
//            }
//            let request = Twitter.sharedInstance().APIClient.URLRequestWithMethod("GET", URL: statusesShowEndpoint, parameters: self.params, error: NSErrorPointer())
//            
//            
//            Twitter.sharedInstance().APIClient.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
//                do {
//                    let json: AnyObject?  = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
//                    let jsonDictionary = json as! [String:AnyObject]
//                    let jsonTweets = jsonDictionary["statuses"] as! [AnyObject]
//                    let tweets = TWTRTweet.tweetsWithJSONArray(jsonTweets) as! [TWTRTweet]
//                    self.tweets = tweets
//                    print(jsonDictionary["statuses"])
//                    self.tableView.reloadData()
//                    sender?.endRefreshing()
//                } catch {
//                    print(connectionError)
//                }
//            }
//        }
        
        
        
        
        
        
        
        
        
        
    }
//                let request = Twitter.sharedInstance().APIClient.URLRequestWithMethod(
//                    "GET", URL: statusesShowEndpoint, parameters: self.params)
//                if request != nil {
//                    Twitter.sharedInstance().APIClient.sendTwitterRequest(request) {
//                        (response, data, connectionError) -> Void in
//                        if (connectionError == nil) {
//                            var jsonError : NSError?
//                            //                            println(data)
//                            var rootData: JSON? = JSON(data: data)
//                            if let data = rootData {
//                                //                                println(data["statuses"])
//                            }
//                            //                            println(jj["statuses"])
//                            let json: AnyObject?  = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &jsonError)
//                            
//                            let jsonDictionary = json as! [String:AnyObject]
//                            // Extract the Tweets and create Tweet objects from the JSON data.
//                            let jsonTweets = jsonDictionary["statuses"] as! [AnyObject]
//                            let tweets = TWTRTweet.tweetsWithJSONArray(jsonTweets) as! [TWTRTweet]
//                            self.tweets = tweets
//                            print(jsonDictionary["statuses"])
//                            self.tableView.reloadData()
//                            
//                            
//                        } else {
//                            print("Error: \(connectionError)")
//                        }
//                    }
//                    sender?.endRefreshing()
//                } else {
//                    sender?.endRefreshing()
//                    print("Error: \(self.clientError)")
//                }
//                
//            }
//        }
//    }
    

    func refresh() {
        refreshControl?.beginRefreshing()
        refresh(refreshControl!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.tabBar.hidden = true
        // Register the cell clas as reuse identifier
        self.title = "Twitter"
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
//        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]

        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension // Explicitly set on iOS 8 if using automatic row height calculation
        tableView.allowsSelection = false
        tableView.registerClass(TWTRTweetTableViewCell.self, forCellReuseIdentifier: tweetTableReuseIdentifier)
        refresh()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if tabBarController?.tabBar.hidden == true {
            
            tabBarController?.tabBar.hidden = false
            print("tabbar.hidden = \(tabBarController?.tabBar.hidden)")
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        if tabBarController?.tabBar.hidden == false {
            tabBarController?.tabBar.hidden = true
        }
    }
    

    func alertWithTitle(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets.count
    }
    
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == tweets.count-1 {
                print("last tweets is loaded you should call api again")
        }
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let tweet = self.tweets[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier(tweetTableReuseIdentifier, forIndexPath: indexPath) as! TWTRTweetTableViewCell
        
        cell.configureWithTweet(tweet)
        cell.tweetView.delegate = self
        
        return cell
    }
}