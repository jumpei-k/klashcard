//
//  KlashCard.swift
//  
//
//  Created by Jumpei Katayama on 7/13/15.
//
//

import Foundation
import CoreData

class Kanji: NSManagedObject {

    @NSManaged var createdDate: NSDate
    @NSManaged var practice: NSNumber
    @NSManaged var kanji: String
    @NSManaged var yomi: String
    
    // TODO: Kanji should have yomi in alphabet
    func loadFrom(dic: [String: AnyObject]) {
        let k = dic["kanji"] as? String
        let y = dic["yomi_jp"] as? String
        let l = dic["level"] as? String
        let u = dic["unit"] as? Int
        let p = dic["inDeck"] as? NSNumber
        let e = dic["meaning"] as? String
        kanji = k!
        yomi = y!
        level = l!
        unit = u!
        practice = p!
        english = e!
        createdDate = NSDate()
    }
    @NSManaged var level: String
    @NSManaged var unit: NSNumber
}

extension Kanji {
    
    @NSManaged var alphabet: String?
    @NSManaged var english: String?
    
}

