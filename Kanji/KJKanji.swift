//
//  File.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/15/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

class KJKlashCard: NSObject {
    var kanji: String?
    var level: String?
    var unit: Int?
    var yomi: String?
//    var japanese: [String?]
    
    init(kanji:String, yomi: String, level: String, unit: Int) {
        super.init()
        self.kanji = kanji
        self.yomi = yomi
        self.level = level
        self.unit = unit
//        self.japanese = japanese
    }
}
