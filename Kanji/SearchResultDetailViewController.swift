//
//  SearchResultDetailViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import AVFoundation


class SearchResultDetailViewController: UIViewController {

    @IBOutlet weak var kanjiLabel: UILabel!
    @IBOutlet weak var yomiLabel: UILabel!
    @IBOutlet weak var meaningLabel: UILabel!
    
    var kanji: Kanji?
    
    var speechSynthesizer = AVSpeechSynthesizer()
    @IBAction func searchOnTwitter(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let twwtvc = storyboard.instantiateViewControllerWithIdentifier("TwitterFeedViewController") as! TwitterFeedViewController
        twwtvc.query = kanji!.kanji
        
        twwtvc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(twwtvc, animated: true)
    }
    
    @IBAction func searchOnGoogleImage(sender: AnyObject) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gglvc = storyboard.instantiateViewControllerWithIdentifier("GoogleImageView") as! GoogleImageViewController
        gglvc.keyword = kanji?.kanji
//        let gglvc = GoogleImageViewController(keyword: self.kanji!.kanji)
        navigationController?.pushViewController(gglvc, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)
        navigationController?.navigationBar.tintColor = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)

        
        tabBarController?.tabBar.hidden = true
        kanjiLabel.text = kanji?.kanji
        yomiLabel.text = kanji?.yomi

//        meaningLabel.text = kanji?.meaning
    }
    override func viewWillDisappear(animated: Bool) {
        tabBarController?.hideTabBar()
    }

    @IBAction func playSound(sender: AnyObject) {
        let utter = AVSpeechUtterance(string:kanji!.kanji)
        // println(AVSpeechSynthesisVoice.speechVoices())
        let v = AVSpeechSynthesisVoice(language: "ja-JP")
        utter.voice = v
        var rate = AVSpeechUtteranceMaximumSpeechRate - AVSpeechUtteranceMinimumSpeechRate
        rate = rate * 0.15 + AVSpeechUtteranceMinimumSpeechRate
        utter.rate = rate
        speechSynthesizer.delegate = self

        speechSynthesizer.speakUtterance(utter)
    }
}

extension SearchResultDetailViewController: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didStartSpeechUtterance utterance: AVSpeechUtterance) {
        print("starting")
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance) {
        print("finished")
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        let s = (utterance.speechString as NSString).substringWithRange(characterRange)
        print("about to say \(s)")
    }
    
}