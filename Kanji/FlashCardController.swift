//
//  FlashCardController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 1/7/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import UIKit

/// FlashCardController is model Controller for page style UI
class FlashCardController: NSObject, UIPageViewControllerDataSource {
    
    var kanjiData: [Kanji]! {
        didSet {
            print("kanjiData: \(kanjiData.count)")
        }
    }
    
    override init() {
        super.init()
    }
    
    func viewControllerAtIndex(index: Int, storyboard: UIStoryboard) -> FlashCardContentViewController? {
        // Return the data view controller for the given index.
        if (self.kanjiData.count == 0) || (index >= self.kanjiData.count) {
            return nil
        }
        
        // Create a new view controller and pass suitable data.
        let pageContentsViewController = storyboard.instantiateViewControllerWithIdentifier("FlashCardContentViewController") as! FlashCardContentViewController
        print("new pagecontent object was created")
        pageContentsViewController.flashCardData = self.kanjiData[index]
        return pageContentsViewController
    }
    
    func indexOfViewController(viewController: FlashCardContentViewController) -> Int {
        return kanjiData.indexOf(viewController.flashCardData) ?? NSNotFound
    }
    
    // MARK: - Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! FlashCardContentViewController)
        if (index == 0) || (index == NSNotFound) {
            return nil
        }
        
        index--
        return self.viewControllerAtIndex(index, storyboard: viewController.storyboard!)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! FlashCardContentViewController)
        if index == NSNotFound {
            return nil
        }
        
        index++
        if index == self.kanjiData.count {
            return nil
        }
        return self.viewControllerAtIndex(index, storyboard: viewController.storyboard!)
    }
}
