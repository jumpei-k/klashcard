//
//  PracticeTableViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/1/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreData
import CoreStore

struct Stack {
    static let KanjiStack: DataStack = {
        
        let dataStack = DataStack(modelName: "KlashCard", migrationChain: ["KlashCard", "KlashCard 2", "KlashCard 3", "KlashCard 4" ]) //migrationChain
        try! dataStack.addSQLiteStoreAndWait(fileName: "kanji.sql")

        
        return dataStack
    }()
}

class PracticeTableViewController: UITableViewController {
    
    var kanjiItems: [Kanji]!//    var fetchResultController: NSFetchedResultsController?

    override func viewDidLoad() {
        super.viewDidLoad()
        kanjiItems = Stack.KanjiStack.fetchAll(From(Kanji), Where("practice == true"), OrderBy(.Ascending("kanji")))!

        title = "Practice"
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.showTabBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.hideTabBar()

    }
    
    // ===============================
    // MARK: - Table View Data Source
    // ===============================
    func numberOfRowsInSection(section: Int) -> Int {
        return 1
    }

    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let info = self.fetchResultController!.sections![section] as NSFetchedResultsSectionInfo
        return kanjiItems.count
        
    }
    

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("practice cell", forIndexPath: indexPath) as UITableViewCell
        let kanji = kanjiItems[indexPath.row]
        cell.textLabel?.text = kanji.kanji
        return cell
    }
    
    
    // ===============================
    // MARK: - Table View Delegate
    // ===============================
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
//        let fcViewCtl = FlashCardViewController(row: indexPath.row, kanjiList: self.fetchResultController?.fetchedObjects as! [Kanji])
        let fcViewCtl = storyboard?.instantiateViewControllerWithIdentifier("FlashCardViewController") as! FlashCardViewController
//        fcViewCtl.kanjiSet = fetchResultController?.fetchedObjects as! [Kanji]
        fcViewCtl.pageIndex = indexPath.row
        navigationController?.pushViewController(fcViewCtl, animated: true)
        //        navc.pushViewController(fcViewCtl, animated: true)
        
        print("select")
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let record: Kanji? = kanjiItems[indexPath.row] //self.fetchResultController?.objectAtIndexPath(indexPath) as? Kanji
            Stack.KanjiStack.beginSynchronous { (transaction) -> Void in
                let r = transaction.edit(record)!
                r.practice = 0
                transaction.commit()
                tableView.reloadData()
//                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)

            }
            if record!.practice.boolValue {

            }
        }
    }
    
    // Individual rows can opt out of having the -editing property set for them. If not implemented, all rows are assumed to be editable.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    /// ========
    /// Private
    ///=========
    
    /************************************
    - TODO: - CreateCustom TabbarItem
    
        tabbarItem:
            two types of image: one is for normalState, the other is for selectedState
            size: 50 * 50
    *************************************/
    
    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath?) {
        // fetch record
//        var record: UITableViewCell = self.fetchResultController?.objectAtIndexPath(indexPath!) as! UITableViewCell
//        var record: UITableViewCell = self.fetchResultController?.objectAtIndexPath(indexPath!) as! UITableViewCell
//        KanjiItems

        
    }
    
//    func setupFetchResultController() {
//        let fetchRequest = NSFetchRequest(entityName: "Kanji")
//        // NSPreicate
//        let predicate_inDeck = NSPredicate(format: "inDeck == 1")
//        fetchRequest.predicate = predicate_inDeck
//        
//        // TODO: - add new field "inDeck_addedDate"
//        //          sort by the date
//        
//        let sort = NSSortDescriptor(key: "kanji", ascending: true)
//        fetchRequest.sortDescriptors = [sort]
//        fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.moc, sectionNameKeyPath: nil, cacheName: nil)
//        fetchResultController?.delegate = self
//        
//        //Perform Fetch
//        do {
//            try fetchResultController?.performFetch()
//            print("Fetch Success")
//        } catch {
//            print(error)
//        }
//    }
}

// ==========================================
// MARK: - NSFetchedResultsControllerDelegate
// ==========================================
extension PracticeTableViewController: NSFetchedResultsControllerDelegate{

    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }

    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
            break
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
            break
        case .Update:
            let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath!)!
            configureCell(cell, atIndexPath: indexPath)
            break
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
            break
            
        }
    }
    
}
