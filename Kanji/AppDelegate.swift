//
//  AppDelegate.swift
//  KlashCard
//
//  Createdc by Jumpei Katayama on 1/28/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import TwitterKit
import CoreStore



let defaults = NSUserDefaults.standardUserDefaults()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
//    migrationChain: ["MyStore", "MyStoreV2", "MyStoreV3"]
//    )
//    var alphabetReading_support: Bool! = NSUserDefaults.standardUserDefaults().valueForKey("alphabetReading_support") as! Bool ?? true {

//    lazy var stack = CoreDataStack(modelName: "KlashCard", storeName: "KlashCard", options:[NSMigratePersistentStoresAutomaticallyOption: true,
//        NSInferMappingModelAutomaticallyOption: true])

//    let twift = Twift(consumerKey: "x7ZGW1EC2c5HsNXwHBYbkOGVV", consumerSecret: "KIEvvQX7FcRrhiQVacJTu3zeCO2Wwx39rk0lma1Yn8sftK6a5T")

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Twitter.sharedInstance().startWithConsumerKey("x7ZGW1EC2c5HsNXwHBYbkOGVV", consumerSecret: "KIEvvQX7FcRrhiQVacJTu3zeCO2Wwx39rk0lma1Yn8sftK6a5T")
        Fabric.with([Twitter.self()])

        if let _ = defaults.stringForKey("hasLaunchedOnce") {
            print("This is not initial launch")
        } else {
            print("App launched first time")
            
            defaults.setBool(true, forKey: "hasLaunchedOnce") // At the first launch, populate data
            defaults.setBool(true, forKey: "alphabetReading_support") // enable alphabetReading_support
            defaults.setBool(true, forKey: "requireInitialData")
            defaults.synchronize()
        }

        return true
    }

    
    func applicationWillTerminate(application: UIApplication) {
        print(NSUserDefaults.standardUserDefaults())
//        self.saveContext()

    }
    
//    func dataPopulation() {
//        // Pupulate all KlashCard Data
//        let rawData = DataManager.getKlashCardData()
//        let json = JSON(data: rawData!)
//        
//        if let allKlashCardData = json["data"].array {
//            for kanji in allKlashCardData {
//
//                let newKj: KlashCard = KlashCard.createInManagedObjectContext(managedObjectContext, kanji: kanji["kanji"].string!, level: kanji["level"].string!, unit: kanji["unit"].int!, inDock: false, createdDate: NSDate(), yomi_jp: kanji["yomi_jp"].string!, meaning: kanji["meaning"].string!)
//            }
//        }
//    }

    
//    func deleteAlRecords() {
//        // Eng
//        let fetchReq_Jp = NSFetchRequest(entityName: "Japanese")
//        fetchReq_Jp.includesPropertyValues = false
//        
//        do {
//            var japs = try managedObjectContext.executeFetchRequest(fetchReq_Jp)
////            var j: Japanese!
//            for j in japs {
//                managedObjectContext.deleteObject(j as! Japanese)
//            }
//            japs.removeAll(keepCapacity: false)
//            try managedObjectContext.save()
//
//        } catch {
//            
//        }
//
//        // KlashCard
//        let fetchReq = NSFetchRequest(entityName: "KlashCard")
//        fetchReq.includesPropertyValues = false
//        
//        do {
//            var kanjis = try managedObjectContext.executeFetchRequest(fetchReq)
//            for kj in kanjis {
//                managedObjectContext.deleteObject(kj as! KlashCard)
//            }
//            kanjis.removeAll(keepCapacity: false)
//            try managedObjectContext.save()
//            
//        } catch {
//            print(error)
//        }
//    }

    func alertWithTitle(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))

    }

}

