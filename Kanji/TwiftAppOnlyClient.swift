//
//  TwiftAppOnlyClient.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

internal class TwiftAppOnlyClient: TwiftClientProtocol {
    
    var consumerKey: String
    var consumerSecret: String
    
    var credential: TwiftCredential?
    
    var dataEncoding: NSStringEncoding
    
    init(consumerKey: String, consumerSecret: String) {
        self.consumerKey = consumerKey
        self.consumerSecret = consumerSecret
        self.dataEncoding = NSUTF8StringEncoding
    }
    
    func get(path: String, baseURL: NSURL, parameters: Dictionary<String, AnyObject>, uploadProgress: TwiftHTTPRequest.UploadProgressHandler?, downloadProgress: TwiftHTTPRequest.DownloadProgressHandler?, success: TwiftHTTPRequest.SuccessHandler?, failure: TwiftHTTPRequest.FailureHandler?) -> TwiftHTTPRequest {
        let url = NSURL(string: path, relativeToURL: baseURL)
        let method = "GET"
        
        let request = TwiftHTTPRequest(URL: url!, method: method, parameters: parameters)
        request.downloadProgressHandler = downloadProgress
        request.successHandler = success
        request.failureHandler = failure
        request.dataEncoding = self.dataEncoding
        
        if let bearerToken = self.credential?.accessToken?.key {
            request.headers = ["Authorization": "Bearer \(bearerToken)"];
        }
        
        request.start()
        return request
    }
    
    func post(path: String, baseURL: NSURL, parameters: Dictionary<String, AnyObject>, uploadProgress: TwiftHTTPRequest.UploadProgressHandler?, downloadProgress: TwiftHTTPRequest.DownloadProgressHandler?, success: TwiftHTTPRequest.SuccessHandler?, failure: TwiftHTTPRequest.FailureHandler?) -> TwiftHTTPRequest {
        let url = NSURL(string: path, relativeToURL: baseURL)
        let method = "POST"
        
        let request = TwiftHTTPRequest(URL: url!, method: method, parameters: parameters)
        request.downloadProgressHandler = downloadProgress
        request.successHandler = success
        request.failureHandler = failure
        request.dataEncoding = self.dataEncoding
        
        if let bearerToken = self.credential?.accessToken?.key {
            request.headers = ["Authorization": "Bearer \(bearerToken)"];
        }
        else {
            let basicCredentials = TwiftAppOnlyClient.base64EncodedCredentialsWithKey(self.consumerKey, secret: self.consumerSecret)
            request.headers = ["Authorization": "Basic \(basicCredentials)"];
            request.encodeParameters = true
        }
        
        request.start()
        return request
    }
    
    class func base64EncodedCredentialsWithKey(key: String, secret: String) -> String {
        let encodedKey = key.urlEncodedStringWithEncoding(NSUTF8StringEncoding)
        let encodedSecret = secret.urlEncodedStringWithEncoding(NSUTF8StringEncoding)
        let bearerTokenCredentials = "\(encodedKey):\(encodedSecret)"
        if let data = bearerTokenCredentials.dataUsingEncoding(NSUTF8StringEncoding) {
            return data.base64EncodedStringWithOptions(nil)
        }
        return String()
    }
    

}