//
//  LoadingDataViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 2/22/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreStore
private let jsonFileName = "kanji_all_data"

class LoadingDataViewController: UIViewController {

    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    let dataStack = DataStack(modelName: "KlashCard")

    
    var kanjiDataRows: [AnyObject]?
    var calucuratedTotalData: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(animated: Bool) {
        let requireInitialData = defaults.objectForKey("requireInitialData") as! Bool
        if requireInitialData {
            startInitialOperation()
        } else {
            print("unknow error occuered")
        }
    }
    
    private func prepareDataStack() {
        try! dataStack.addSQLiteStoreAndWait(fileName: "kanji.sql")
    }
    
    private func startInitialOperation() {
        progressLabel.text = "Initializing data..."
        indicator.startAnimating()
        let queue = NSOperationQueue()
        progressView.progress = 0
        let retrieveJSONData = NSBlockOperation(block: {
            let jsonFilePath = NSBundle.mainBundle().pathForResource(jsonFileName, ofType:"json")
            let buffer = DataManager.openJSON(jsonFilePath!)!
            let json = try! NSJSONSerialization.JSONObjectWithData(buffer, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
            self.kanjiDataRows = json["data"] as? [AnyObject]
            
        })
        
        retrieveJSONData.completionBlock = {
            print("Complete retrieveJSONData")
        }
        
        let dataPopulation = NSBlockOperation(block: {
            if let allRows = self.kanjiDataRows {
                self.prepareDataStack()
                self.calucuratedTotalData = allRows.count
                print("found \(allRows.count) rows")
                for row in allRows {
                    _ = self.dataStack.beginSynchronous({ (transaction) -> Void in
                        let kanjiRow = row as! [String: AnyObject]
                        let kanji = transaction.create(Into(Kanji))
                        kanji.loadFrom(kanjiRow)
                        transaction.commit()
                        self.updateProgressBy(Float(1)/Float(allRows.count))

//                        NSOperationQueue.mainQueue().addOperationWithBlock({
//                        })

                    })
                }
            }
        })
        
        
        
        dataPopulation.completionBlock = {
            print("Complete dataPopulation")
            if self.calucuratedTotalData == self.dataStack.fetchAll(From(Kanji))?.count {
                defaults.setBool(false, forKey: "requireInitialData")
                defaults.synchronize()
                self.indicator.stopAnimating()
                print("now ready to use!")
                self.dismissViewControllerAnimated(true, completion: nil)
            } else {
                print("unknowErrorOccur")
            }
        }
        
        dataPopulation.addDependency(retrieveJSONData)
        
        queue.addOperations([retrieveJSONData, dataPopulation], waitUntilFinished: false)
        
    }
    
    private func saveOneKanji(rows: [AnyObject]) {
        
    }
    
    private func saveOneData(row: JSON) {
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateProgressBy(scale: Float) {
        progressView.progress += scale
        let progressValue = self.progressView?.progress
        let percentage = progressValue! * 100
        let formatedValue = String.localizedStringWithFormat("%.2f", percentage)
        print("進捗度: \(formatedValue)%")
//        String.localizedStringWithFormat("%.2f %@", value, unit)
        progressLabel?.text = "\(formatedValue)%"
    }

}
