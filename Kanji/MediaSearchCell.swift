//
//  MediaSearchCell.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 6/22/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class MediaSearchCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
