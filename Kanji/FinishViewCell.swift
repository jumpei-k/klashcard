//
//  FinishViewCell.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 4/9/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class FinishViewCell: UITableViewCell {
//    var imgUser    = UIImageView();
    var kanji = UILabel();
    var yourAns = UILabel();
    var judgeImg = UIImageView()
    var correctAns = UILabel();
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
//        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
//        imgUser.clipsToBounds = true;
//        kanji = UILabel(frame: CGRectMake(70, 10, self.bounds.size.width - 40 , 25))
        kanji = UILabel(frame: CGRectMake(70, 10, 0 , 0))
        
        kanji.backgroundColor = UIColor.purpleColor()
        
        yourAns = UILabel(frame: CGRectMake(130, 10, 0 , 0))
        yourAns.backgroundColor = UIColor.purpleColor()
        
        correctAns = UILabel(frame: CGRectMake(200, 10, 0 , 0))
        correctAns.backgroundColor = UIColor.orangeColor()
        
        judgeImg = UIImageView(frame: CGRectMake(0, 0, 50, 50))
        judgeImg.backgroundColor = UIColor.lightGrayColor()
        
        
        contentView.addSubview(judgeImg)
        
//        yourAns = UILabel(frame: CGRectMake(20, 10, self.bounds.size.width - 40 , 25))
//        correctAns = UILabel(frame: CGRectMake(20, 10, self.bounds.size.width - 40 , 25))
        
        contentView.addSubview(kanji)
        contentView.addSubview(yourAns)
        contentView.addSubview(correctAns)
        
//        self.frame.size.height = 100
//        contentView.addSubview(labTime)
        
        
        //Set layout
//        var viewsDict = Dictionary <String, UIView>()
//        viewsDict["image"] = imgUser;
//        viewsDict["username"] = labUerName;
//        viewsDict["message"] = labMessage;
//        viewsDict["time"] = labTime;
        //Image
        
        //contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[image(100)]-'", options: nil, metrics: nil, views: viewsDict));
        //contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[image(100)]-|", options: nil, metrics: nil, views: viewsDict));
//        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[username]-[message]-|", options: nil, metrics: nil, views: viewsDict));
//        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[username]-|", options: nil, metrics: nil, views: viewsDict));
//        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-[message]-|", options: nil, metrics: nil, views: views
//        Dict));
        
        // disapble highlight
        self.userInteractionEnabled = false;
//        self.textLabel.enabled = false;
//        self.detailTextLabel.enabled = false;
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        kanji = UILabel(frame: CGRectMake(20, 10, self.bounds.size.width - 40 , 25))
//        yourAns = UILabel(frame: CGRectMake(20, 10, self.bounds.size.width - 40 , 25))
//        correctAns = UILabel(frame: CGRectMake(20, 10, self.bounds.size.width - 40 , 25))
//        
//    }
}
