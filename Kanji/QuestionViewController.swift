//
//  KNextViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 4/3/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    
    var kquestion: Question?
    var kview: Kview?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.title = "KNext"
//        kview = Kview(frame: UIScreen.mainScreen().bounds)
        kview = Kview(frame: CGRectZero)
//        kview!.backgroundColor = UIColor.cyanColor()
        println("\(NSStringFromClass(self.dynamicType)): initilized")

    }
    
    convenience init(question: Question) {
        self.init(nibName: nil, bundle: nil)
        kquestion = question
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// Lyfe cycle
    
    override func loadView() {
        println("\(NSStringFromClass(self.dynamicType)): loadView is called")
        super.loadView()
//        kview?.frame = view.bounds
//        view = kview!
        kview!.backgroundColor = UIColor.cyanColor()
        
        self.kview!.choice1?.setTitle(kquestion?.choices[0]!, forState: UIControlState.Normal)
        self.kview!.choice2?.setTitle(kquestion?.choices[1]!, forState: UIControlState.Normal)
        self.kview!.choice3?.setTitle(kquestion?.choices[2]!, forState: UIControlState.Normal)
        self.kview!.choice4?.setTitle(kquestion?.choices[3]!, forState: UIControlState.Normal)
        
        self.kview!.questionLabel?.text = kquestion?.question
        view.addSubview(kview!)
        // layout subviews
    }
    
    override func viewDidLayoutSubviews() {
        println("\(NSStringFromClass(self.dynamicType)): viewDidLayoutSubviews")
        super.viewDidLayoutSubviews()
        self.kview?.frame = view.bounds

//        self.kview?.updateConstraints()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
}
