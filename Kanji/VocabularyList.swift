//
//  VocabularyList.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/1/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreData
import CoreStore

class VocabularyList: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var unitNumber: Int?//    var myLevel: String!
    var level: String!
    var kanjiItems: [Kanji]?

    var alphabetReading_support: Bool? = NSUserDefaults.standardUserDefaults().valueForKey("alphabetReading_support") as? Bool{
        didSet {
            tableView.reloadData()
        }
    }

//    var fetchResultController: NSFetchedResultsController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register the cell clas as reuse identifier
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
//        myLevel = level
        let titleStr = self.level!
        if let unum = unitNumber {
            navigationItem.title = level! + " " + String(unitNumber!)
        } else {
            navigationItem.title = titleStr
        }
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        kanjiItems = Stack.KanjiStack.fetchAll(
            From(Kanji),
            Where("level MATCHES %@", level!) && Where("unit == %d", unitNumber!),
            OrderBy(.Ascending("kanji")))
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("\(NSStringFromClass(self.dynamicType)): ViewwWIllApper")
        
        // chaeck if userdefault exsists
        print(NSUserDefaults.standardUserDefaults().valueForKey("alphabetReading_support")) // return optional int: 0 or 1
        alphabetReading_support = (NSUserDefaults.standardUserDefaults().valueForKey("alphabetReading_support") as? Bool)!
        tabBarController?.showTabBar()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.hideTabBar()

    }
    
    
    //=======
    //Private
    //=======

//    private func setupFetchResultController() {
//        let fetchRequest = NSFetchRequest(entityName: "Kanji")
//        // NSPreicate
//        let predicate_level = NSPredicate(format: "level MATCHES %@", level)
//        var compoundPredicate: NSCompoundPredicate?
//        if let num = unitNumber {
//            print("unit = \(num) and level: =\(level)")
//            let predicate_unit = NSPredicate(format: "unit == %d", num)
//            compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate_level, predicate_unit])
////            compoundPredicate = NSCompoundPredicate.andPredicateWithSubpredicates([predicate_level, predicate_unit])
//        } else {
//            compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate_level])
//        }
////        println("level: \(predicate_level), unit: \(predicate_unit)")
//
//        fetchRequest.predicate = compoundPredicate!
//        
//        // SortDescriptor
//        let sort = NSSortDescriptor(key: "kanji", ascending: true)
//        fetchRequest.sortDescriptors = [sort]
////        self.fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.moc, sectionNameKeyPath: nil, cacheName: nil)
////        fetchResultController?.delegate = self
//        
//        //Perform Fetch
//        do {
//            try fetchResultController?.performFetch()
//        } catch {
//            print("Unable to perform fetch.")
//            print(error)
//        }
//    }

}

extension UITabBarController {
    func showTabBar() {
        if tabBar.hidden == true {
            tabBar.hidden = false
        }
    }
    
    func hideTabBar() {
        if tabBar.hidden == false {
            tabBar.hidden = true
        }
    }
}

// ==========================================
// MARK: - NSFetchedResultsControllerDelegate
// ==========================================


extension VocabularyList: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }

}


// ===============================
// MARK: - Table View Data Source
// ===============================

extension VocabularyList: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }// Default is 1 if not implemented

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        let info = self.fetchResultController!.sections![section] as NSFetchedResultsSectionInfo
        return (kanjiItems?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("kanji list cell", forIndexPath: indexPath) as! VovabularyTableViewCell
//        let kanji = self.fetchResultController?.objectAtIndexPath(indexPath) as! Kanji
        let kanji = kanjiItems![indexPath.row]
        
        cell.loadDataForCell(kanji, english_support: alphabetReading_support!)
        return cell
    }
}

// ===============================
// MARK: - Table View Delegate
// ===============================
extension VocabularyList: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let fcViewCtl = storyboard.instantiateViewControllerWithIdentifier("FlashCardViewController") as! FlashCardViewController
        fcViewCtl.pageIndex = indexPath.row
        fcViewCtl.kanjiSet = kanjiItems
        navigationController?.pushViewController(fcViewCtl, animated: true)
    }

    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? { // supercedes -tableView:titleForDeleteConfirmationButtonForRowAtIndexPath: if return value is non-nil
        let addAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "Add" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
            
            let kanji = self.kanjiItems![indexPath.row]// self.fetchResultController?.objectAtIndexPath(indexPath) as! Kanji
            if kanji.practice.boolValue == false {
                print("hey")
                Stack.KanjiStack.beginSynchronous { (transaction) -> Void in
                    let k = transaction.edit(kanji)!
                    k.practice = 1
                    transaction.commit()
                    print("yo")
                    tableView.reloadData()
                    dispatch_async(dispatch_get_main_queue(), {

                    })
                    

                }
            }
            
            
            
        })
        addAction.backgroundColor = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)
        
        return [addAction]
    }

}