//
//  FlashCardViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/1/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

/// FlashCardViewController is a page container class which takes care of page content and model controller
class FlashCardViewController: UIViewController {

    var kanjiSet: [Kanji]!
    var pageIndex: Int!
    
    lazy var pageViewController: UIPageViewController = {
        let pvc = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        pvc.dataSource = self.flashCardController
        pvc.delegate = self
        return pvc
    }()
    
    lazy var flashCardController: FlashCardController = {
        let fcdCtl = FlashCardController()
        fcdCtl.kanjiData = self.kanjiSet
        return fcdCtl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.hidden = true
        view.backgroundColor = UIColor.lightGrayColor()
        print("\(NSStringFromClass(self.dynamicType)): viewDidLoad is called")
        automaticallyAdjustsScrollViewInsets = false
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let startingViewController: FlashCardContentViewController = flashCardController.viewControllerAtIndex(pageIndex, storyboard: storyboard)!
        let viewControllers = [startingViewController]
        
        self.pageViewController.setViewControllers(viewControllers, direction: .Forward, animated: true, completion: nil)
        
        addChildViewController(self.pageViewController)
        view.addSubview(pageViewController.view)
        let pageViewRect = view.bounds
        pageViewController.view.frame = pageViewRect
        self.pageViewController.didMoveToParentViewController(self)
    }
}

//======================================
// MARK: UIPageViewControllerDelegate
//======================================
extension FlashCardViewController: UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        print("didFinishAnimating")
        
    }
}
