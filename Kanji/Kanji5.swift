//
//  KlashCard.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/23/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation
import CoreData

class KlashCard: NSManagedObject {

    @NSManaged var inDeck: NSNumber
    @NSManaged var kanji: String
    @NSManaged var level: String
    @NSManaged var unit: NSNumber
    @NSManaged var yomi: String
    @NSManaged var createdDate: NSDate?
    @NSManaged var japanese: NSSet

    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, kanji: String, level: String, unit: NSNumber, yomi: String, inDock: NSNumber = false, createdDate: NSDate? = nil, japanese: NSSet) -> KlashCard {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("KlashCard", inManagedObjectContext: moc) as! KlashCard
        newItem.kanji = kanji
        newItem.level = level
        newItem.unit = unit
        newItem.yomi = yomi
        newItem.inDeck = inDock
        newItem.createdDate = nil
        newItem.japanese = japanese
        
        return newItem
    }

}
