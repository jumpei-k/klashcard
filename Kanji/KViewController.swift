//
//  KViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 4/2/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

/// KviewController is a game container
/// It contains questionContrrollers
/// Depending on the level the questions change

import UIKit


class KViewController: UIViewController {
    
    var level: QuizLevel?
    var cancelButton: UIButton?
    
    let questions = (UIApplication.sharedApplication().delegate as! AppDelegate).questions
    var userAnsers = (UIApplication.sharedApplication().delegate as! AppDelegate).userAnsers
    var currentQuestionViewCtl: QuestionViewController?
    var nextQuestionViewCtl: QuestionViewController?

    var questionViewFrame: CGRect = CGRect()
    var endViewCtl: FinishViewController?
    var numberOfQuestions: Int?
    var currentQuestionNumber: Int?
    var questionVCs:[QuestionViewController]?
    var isLastQuestion: Bool = false

    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.title = "Home"
        numberOfQuestions = questions.count
        println("\(NSStringFromClass(self.dynamicType)): initilized")
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(level: QuizLevel) {
        self.init(nibName: nil, bundle: nil)
        self.level = level
        self.setCancelButton()
        self.setupQuestions()
    }
    
    /// Lyfe cycle
    override func loadView() {
        println("\(NSStringFromClass(self.dynamicType)): loadview is called")
        super.loadView()
        view.backgroundColor = UIColor.orangeColor()
        self.questionViewFrame = view.bounds
        
        view.addSubview(self.currentQuestionViewCtl!.view)
        view.addSubview(self.cancelButton!)
        // layout subviews
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("\(NSStringFromClass(self.dynamicType)): viewDidLoad is called")
    }
    
    override func viewDidLayoutSubviews() {
        println("\(NSStringFromClass(self.dynamicType)): viewDidLayoutSubviews is called")
        super.viewDidLayoutSubviews()
        self.currentQuestionViewCtl?.view?.frame = CGRectMake(self.questionViewFrame.origin.x, self.questionViewFrame.origin.y + 100, self.questionViewFrame.size.width, self.questionViewFrame.size.height-120)
        view.addSubview(self.cancelButton!)
        self.cancelButton!.addTarget(self, action: "cancel", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        println("\(NSStringFromClass(self.dynamicType)): viewWillAppear is called")
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupQuestions() {
        self.currentQuestionNumber = 1
        self.isLastQuestion = false
        self.questionVCs = [QuestionViewController]()
        for i in 0..<numberOfQuestions! {
            var qvc = QuestionViewController(question: questions[i]!)
            qvc.kview?.choice1?.addTarget(self, action: ("pushAction:"), forControlEvents: UIControlEvents.TouchUpInside)
            qvc.kview?.choice2?.addTarget(self, action: ("pushAction:"), forControlEvents: UIControlEvents.TouchUpInside)
            qvc.kview?.choice3?.addTarget(self, action: ("pushAction:"), forControlEvents: UIControlEvents.TouchUpInside)
            qvc.kview?.choice4?.addTarget(self, action: ("pushAction:"), forControlEvents: UIControlEvents.TouchUpInside)
            
            self.questionVCs?.append(qvc)
            self.userAnsers.append((i+1, false, questions[i]!.answer!, ""))
            
        }
        self.currentQuestionViewCtl = questionVCs![currentQuestionNumber!-1]
        self.nextQuestionViewCtl = questionVCs![currentQuestionNumber!]
    }
    
    
    func pushAction(sender: AnyObject) {
        // 1. Set the andwer to userAnswer
        // 2. Check the current question is last or not
        //    if so go to review view
        //    otherwise go to next question view
        
        var btn = sender as! UIButton
        var answerId = btn.tag
       
//        println(currentQuestionNumber!)
//        println("buttontag is \(answerId)")
//        println(btn.currentTitle!)
//        
//        println(numberOfQuestions!)
        // Validate the answer is correct
        userAnsers[currentQuestionNumber!-1].3 = btn.currentTitle
        
        if userAnsers[currentQuestionNumber!-1].3 == userAnsers[currentQuestionNumber!-1].2 {
            userAnsers[currentQuestionNumber!-1].1 = true
        }
        
//        println(userAnsers[currentQuestionNumber!-1])

        println("PUSHED")
//        // when the current question is 10, it won't show next question 
        if currentQuestionNumber! >= numberOfQuestions {
            // move feed back view
            endViewCtl?.answerSet = userAnsers
            self.currentQuestionViewCtl!.presentViewController(endViewCtl!, animated: true, completion: {
//                print("last!")
            })
            self.view.addSubview(self.endViewCtl!.view)
//            self.endViewCtl!.view.frame = CGRectInset(self.questionViewFrame, 20, 50)
//            println("This is last Question!!!")
//            println("Current Question NUmber is \(self.currentQuestionNumber!)")

        } else {
            self.currentQuestionViewCtl!.presentViewController(nextQuestionViewCtl!, animated: true, completion: {
                println("Completion success!")})
            self.currentQuestionViewCtl = self.nextQuestionViewCtl
            self.currentQuestionNumber! += 1
            if !(self.currentQuestionNumber == numberOfQuestions) {
                self.nextQuestionViewCtl = self.questionVCs![self.currentQuestionNumber!]
            } else {
                println("last question!")
                endViewCtl = FinishViewController()
            }
//            println("Current Question NUmber is \(self.currentQuestionNumber!)")
            view.addSubview(self.currentQuestionViewCtl!.view)
            
//            self.currentQuestionViewCtl!.view.frame = CGRectInset(self.questionViewFrame, 20, 50)

        }
    }
    
    private func setCancelButton() {
        self.cancelButton = UIButton.buttonWithType(.System) as? UIButton
        self.cancelButton!.setTitle("Cancel", forState: UIControlState.Normal)
        self.cancelButton!.frame = CGRectMake(20, 30, 80, 50)
        self.cancelButton!.backgroundColor = UIColor.whiteColor()
//        self.cancelButton!.addTarget(self, action: "cancel", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func cancel() {
        let homevc = KStartViewController(nibName: nil, bundle: nil)
        self.presentViewController(homevc, animated: true, completion: nil)
        println("waaaaas")
    }
}