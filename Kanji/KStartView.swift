//
//  KStartView.swift
//  Kanji
//
//  Created by Jumpei Katayama on 4/16/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit


class KStartView: UIView {
    var startButton: UIButton?
    var beginnerButton: UIButton?
    var intermediateButton : UIButton?
    var advancedButton : UIButton?
    var nerdButton : UIButton?
    
    var questionLabel: UILabel?
    
    override init(frame: CGRect) { 
        super.init(frame: frame)
        self.backgroundColor = UIColor.darkGrayColor()
        beginnerButton = levelButton("Beginner", tag: 1)
        intermediateButton = levelButton("Intermediate", tag: 2)
        advancedButton = levelButton("Advance", tag: 3)
        nerdButton = levelButton("Nerd", tag: 4)
        startButton = UIButton.buttonWithType(.System) as? UIButton
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
//    override func intrinsicContentSize() -> CGSize {
//        // Default view size
//        super.intrinsicContentSize()
//    }
//    
    override func layoutSubviews() {
        println("\(NSStringFromClass(self.dynamicType)): layoutSubviews is called")
        super.layoutSubviews()
        
//        self.addSubview(startButton!)
        self.addSubview(beginnerButton!)
        self.addSubview(intermediateButton!)
        self.addSubview(advancedButton!)
        self.addSubview(nerdButton!)
    }
    

    override func updateConstraints() {
        println("\(NSStringFromClass(self.dynamicType)): updateConstraints is called")

        super.updateConstraints()
        let views = ["beginner": beginnerButton!, "intermidiate": intermediateButton!, "advance": advancedButton!, "nerd": nerdButton!]
        
        let metrics = ["v1Width": 100, "v1Height": 100, "v2Width": 100, "v2Height": 100, "topMargin": 100, "bottomMargin":70, "sideMargin": 50, "betweenButton": 40]
        
        
        let v1ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-sideMargin-[beginner]-sideMargin-|", options: NSLayoutFormatOptions(0), metrics: metrics, views: views)
        let v2ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-sideMargin-[intermidiate]-sideMargin-|", options: NSLayoutFormatOptions(0), metrics: metrics, views: views)
        
        let v3ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-sideMargin-[advance]-sideMargin-|", options: NSLayoutFormatOptions(0), metrics: metrics, views: views)
        
        let v4ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-sideMargin-[nerd]-sideMargin-|", options: NSLayoutFormatOptions(0), metrics: metrics, views: views)
        
        let vc = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topMargin-[beginner(intermidiate)]-betweenButton-[intermidiate(advance)]-betweenButton-[advance(nerd)]-betweenButton-[nerd]-bottomMargin-|", options: NSLayoutFormatOptions.AlignAllLeading, metrics: metrics, views: views)
        
        self.addConstraints(v1ConstH)
        self.addConstraints(v2ConstH)
        self.addConstraints(v3ConstH)
        self.addConstraints(v4ConstH)
        
        self.addConstraints(vc)
        
        startButton!.frame = CGRectMake(0, 0, 122, 55)
        startButton?.center = CGPointMake(self.frame.size.width/2, 200)
        startButton?.setTitle("Stat", forState: UIControlState.Normal)
        startButton!.backgroundColor = UIColor.yellowColor()

        
    }
    
    override func drawRect(rect: CGRect) {
        println("\(NSStringFromClass(self.dynamicType)): drawRect is called")
        super.drawRect(rect)
        self.updateConstraintsIfNeeded()
    }
    
    
    override func didAddSubview(subview: UIView) {
        println("\(NSStringFromClass(self.dynamicType)): didAddSubview is called")
        super.didAddSubview(subview)
        
    }
    
    
    //==========
    // Private
    //==========
    
    
    private func levelButton(title: String, tag: Int) -> UIButton {
        let btn = UIButton.buttonWithType(.System) as? UIButton
        btn?.frame = CGRectZero
        btn?.tag = tag
        btn?.setTitle(title, forState: UIControlState.Normal)
        btn?.backgroundColor = UIColor.greenColor()
        btn?.setTranslatesAutoresizingMaskIntoConstraints(false)
        return btn!
    }
    
    
}

