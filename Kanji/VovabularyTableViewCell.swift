//
//  VovabularyTableViewCell.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 7/15/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class VovabularyTableViewCell: UITableViewCell {

    @IBOutlet weak var reading_jp: UILabel!
    @IBOutlet weak var kanji: UILabel!
    @IBOutlet weak var meaning: UILabel!
    @IBOutlet weak var reading_en: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    /**
    load model from data
    
    :param: kanji_data_row : Kanji
    */
    func loadDataForCell(kanji_data_row: Kanji, english_support: Bool) {
//        print("load model with english_support: \(english_support)")
        if english_support {
            self.reading_en.text = "coming soon"
        } else {
            self.reading_en.text = ""
        }
        self.kanji.text = kanji_data_row.kanji
        self.reading_jp.text = kanji_data_row.yomi
        return
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
