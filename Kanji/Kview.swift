//
//  Kview.swift
//  Kanji
//
//  Created by Jumpei Katayama on 4/3/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class Kview: UIView {
    var choice1: UIButton?
    var choice2: UIButton?
    var choice3: UIButton?
    var choice4: UIButton?
    
    var questionLabel: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        questionLabel = UILabel(frame: CGRectZero)
        choice1 = choiceButton(1, rect: CGRectZero)
        choice2 = choiceButton(2, rect: CGRectZero)
        choice3 = choiceButton(3, rect: CGRectZero)
        choice4 = choiceButton(4, rect: CGRectZero)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)


        questionLabel!.textColor = UIColor.blackColor()
        questionLabel!.backgroundColor = UIColor.yellowColor()
//        updateConstraintsIfNeeded()
        
        println("\(NSStringFromClass(self.dynamicType)): drawRect")
    }
    
//    override func intrinsicContentSize() -> CGSize {
//            // Default view size
//        super.intrinsicContentSize()
//        let contensSize = CGSize(width: <#CGFloat#>, height: <#CGFloat#>)
//        return
//    }
    
    
    
//    override func updateConstraints() {
//        println("\(NSStringFromClass(self.dynamicType)): updateConstraints is called")
//
//        super.updateConstraints()
//        let views = ["choice1": choice1!, "choice2": choice2!, "choice3": choice3!, "choice4": choice4!]
//        let metrics = ["v1Width": 100, "v1Height": 100, "v2Width": 100, "v2Height": 100, "topMargin": 100, "leftMargin": 30]
//        
//        
//        let v1ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-30-[choice1(==choice2)]-30-[choice2]-30-|", options: nil, metrics: nil, views: views)
//        let v2ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-30-[choice3(==choice4)]-30-[choice4]-30-|", options: nil, metrics: nil, views: views)
//        
//        let vc = NSLayoutConstraint.constraintsWithVisualFormat("V:|-350-[choice1(==choice3)]-30-[choice3]-90-|", options: nil, metrics: nil, views: views)
//        let vc2 = NSLayoutConstraint.constraintsWithVisualFormat("V:|-350-[choice2(==choice4)]-30-[choice4]-90-|", options: nil, metrics: nil, views: views)
//        
//        self.addConstraints(v1ConstH)
//        self.addConstraints(vc)
//        self.addConstraints(v2ConstH)
//        self.addConstraints(vc2)
//    }
    
//    override func setNeedsLayout() {
//        super.setNeedsLayout()
//        println("setNeedsLayout")
//    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        println("\(NSStringFromClass(self.dynamicType)): layoutSubviews")
        
        questionLabel!.textAlignment = .Center
        questionLabel!.font = UIFont(name: "System", size: 60)
        
//        questionLabel?.frame = CGRectMake(0, 0, 140, 140)
//        questionLabel!.center = CGPointMake(self.frame.size.width/2, 200)
//        choice1!.frame = CGRectMake(25, 400, 122, 55)
//        choice2!.frame = CGRectMake(160, 400, 122, 55)
//        choice3!.frame = CGRectMake(25, 500, 122, 55)
//        choice4!.frame = CGRectMake(160, 500, 122, 55)
        self.addSubview(questionLabel!)
        self.addSubview(choice1!)
        self.addSubview(choice2!)
        self.addSubview(choice3!)
        self.addSubview(choice4!)

        self.myConstraints()
    }
    
    func myConstraints() {
        let views = ["choice1": choice1!, "choice2": choice2!, "choice3": choice3!, "choice4": choice4!]
//        let metrics = ["v1Width": 100, "v1Height": 100, "v2Width": 100, "v2Height": 100, "topMargin": 100, "leftMargin": 30]
//        
//        
//        let v1ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-30-[choice1(==choice2)]-30-[choice2]-30-|", options: nil, metrics: nil, views: views)
////        let v2ConstH = NSLayoutConstraint.constraintsWithVisualFormat("H:|-30-[choice3(==choice4)]-30-[choice4]-30-|", options: nil, metrics: nil, views: views)
////        
//        let vc = NSLayoutConstraint.constraintsWithVisualFormat("V:|-350-[choice1(90)]-90-|", options: nil, metrics: nil, views: views)
//        let vc2 = NSLayoutConstraint.constraintsWithVisualFormat("V:|-350-[choice2(90)]-90-|", options: nil, metrics: nil, views: views)
//        let vc2 = NSLayoutConstraint.constraintsWithVisualFormat("V:|-350-[choice2(==choice4)]-30-[choice4]-90-|", options: nil, metrics: nil, views: views)
////        
//        self.addConstraints(v1ConstH)
//        self.addConstraints(vc)
////        self.addConstraints(v2ConstH)
//        self.addConstraints(vc2)
        
        let pbt: CGFloat = 40
        let sm: CGFloat = 50
        let tw: CGFloat = 122           // >= 122
        let scx: CGFloat = self.center.x
        let ssw: CGFloat = self.bounds.size.width
        
        questionLabel?.frame = CGRectMake(0, 0, 140, 140)
        questionLabel!.center = CGPointMake(self.frame.size.width/2, 200)
        choice1!.frame = CGRectMake(0, 0, tw, 55)
        choice1!.center = CGPointMake(sm+tw/2, 320)
        choice2!.frame = CGRectMake(0, 0, tw, 55)
        choice2!.center = CGPointMake(tw/2+scx+pbt/2, 320)
        choice3!.frame = CGRectMake(0, 0, tw, 55)
        choice3!.center = CGPointMake(sm+tw/2, 400)
        choice4!.frame = CGRectMake(0, 0, tw, 55)
        choice4!.center = CGPointMake(tw/2+scx+pbt/2, 400)
        
    }
    
    private func choiceButton(tag: Int, rect: CGRect) -> UIButton {
        let button = UIButton.buttonWithType(.System) as? UIButton
        button?.frame = rect
        button?.tag = tag
        button?.backgroundColor = UIColor.whiteColor()
        return button!
    }
    
}
