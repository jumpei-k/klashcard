//
//  UIColor+Kanji.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/22/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func fromHexRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF000000) >> 24) / 255.0,
            green: CGFloat((rgbValue & 0x00FF0000) >> 16) / 255.0,
            blue: CGFloat((rgbValue & 0x0000FF00) >> 8) / 255.0,
            alpha: CGFloat(rgbValue & 0x000000FF) / 255.0
        )
    }
}