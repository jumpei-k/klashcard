//
//  FlashCardContentView.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 1/7/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import UIKit

class FlashCardContentView: UIView {

    @IBOutlet weak var flashCardView: UIView!
    
    override func drawRect(rect: CGRect) {
        
    }

}


/// Helper method to load UIView forom nib
/// http://stackoverflow.com/questions/25513271/how-to-initialise-a-uiview-class-with-a-xib-file-in-swift-ios/33424509#33424509
protocol UIViewLoading {}
extension UIView : UIViewLoading {}

extension UIViewLoading where Self : UIView {
    
    // note that this method returns an instance of type `Self`, rather than UIView
    static func loadFromNib() -> Self {
        let nibName = "\(self)".characters.split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: nil)
        return nib.instantiateWithOwner(self, options: nil).first as! Self
    }
    
}
