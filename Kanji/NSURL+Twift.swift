//
//  NSURL+Twift.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

extension NSURL {
    
    func URLByAppendingQueryString(queryString: String) -> NSURL {
        if count(queryString.utf16) == 0 {
            return self
        }
        
        var absoluteURLString = self.absoluteString!
        
        if absoluteURLString.hasSuffix("?") {
            absoluteURLString = absoluteURLString[0 ..< count(absoluteURLString.utf16)]
        }
        
        let URLString = absoluteURLString + (absoluteURLString.rangeOfString("?") != nil ? "&" : "?") + queryString
        
        return NSURL(string: URLString)!
    }
    
}
