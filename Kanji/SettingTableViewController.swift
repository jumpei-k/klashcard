//
//  SettingTableViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 7/14/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {

    
    @IBOutlet weak var switch_alphabet_reading: UISwitch!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch_alphabet_reading.on = NSUserDefaults.standardUserDefaults().valueForKey("alphabetReading_support") as! Bool

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    @IBAction func switch_alphabetReading_support(sender: AnyObject) {
        if switch_alphabet_reading.on {
            let userDeafault = NSUserDefaults.standardUserDefaults()
            userDeafault.setBool(switch_alphabet_reading.on, forKey: "alphabetReading_support")
            userDeafault.synchronize()
            print("on")
            
        } else {
            print("off")
            let userDeafault = NSUserDefaults.standardUserDefaults()
            userDeafault.setBool(switch_alphabet_reading.on, forKey: "alphabetReading_support")
            userDeafault.synchronize()
        }
    }

}
