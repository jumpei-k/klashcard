//
//  CoreDataManager.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/9/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation
import CoreData

let ENTITY_NAME = "KlashCard"

//struct CoreDataManager {
//    // moc should be singleton object
//    
//    static func pupulateKlashCard(context: NSManagedObjectContext) {
//        // Pupulate all KlashCard Data
////        let rawData = DataManager.openJSON("kanji_all_data")
//        let json = DataManager.openJSON("kanji_all_data")
//        
//        if let allKlashCardData = json["data"].array {
//            for kanji in allKlashCardData {
//                let newKj: KlashCard = KlashCard.createInManagedObjectContext(context, kanji: kanji["kanji"].string!, level: kanji["level"].string!, unit: kanji["unit"].int!, inDock: false, createdDate: NSDate(), yomi_jp: kanji["yomi_jp"].string!, meaning: kanji["meaning"].string!)
//            }
//        }
//        context.saveContext()
//        
//    }
//    
//    
//    
//    static func checkDataPopulatation() {
//        let flg = defaults.objectForKey("hasPopulatedData") as? Bool
//        if flg! {
//            
//        } else {
//            
//        }
//    }
//    
//
//    static func fetchKlashCard(moc: NSManagedObjectContext, entityName: String = "KlashCard") -> [KlashCard]? {
//        let fetchRequest = NSFetchRequest(entityName: entityName)
//        
//        // Create a sort descriptor object that sorts on the "title"
//        // property of the Core Data object
//        let sortDescriptor = NSSortDescriptor(key: "kanji", ascending: true)
//        
//        // Set the list of sort descriptors in the fetch request,
//        // so it includes the sort descriptor
//        fetchRequest.sortDescriptors = [sortDescriptor]
//        
////        if let fetchResults = moc.executeFetchRequest(fetchRequest, error: nil) as? [KlashCard] {
////            KlashCardItems = fetchResults
////        }
//        do {
//            let kanjis = try moc.executeFetchRequest(fetchRequest) as? [KlashCard]
//            return kanjis
//        } catch {
//            print(error)
//        }
//        return nil
//    }
//}
//
//extension NSManagedObjectContext {
//    func saveContext () {
//        if hasChanges {
//            do {
//                try save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nserror = error as NSError
//                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
//                abort()
//            }
//        }
//    }
//}
