//
//  finishViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 2/23/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class FinishViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let questions = (UIApplication.sharedApplication().delegate as! AppDelegate).questions
    var tableview: UITableView?
    var message: UILabel?
    var homeBUtton: UIButton?
    let numberOfQuestions: Int = 10
    var correctAnswers: Int?
    var wrongAnswers: Int?
    var answerSet = [(Int, Bool, String, String?)]()
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.title = "FINISH"
        tableview = UITableView(frame: CGRectZero, style: .Plain)
        tableview?.dataSource = self
        tableview?.delegate = self
        tableview!.registerClass(FinishViewCell.self, forCellReuseIdentifier: "Cell")
        tableview!.rowHeight = UITableViewAutomaticDimension
//        tableview!.estimatedRowHeight = 80
//        view.addSubview(tableview!)
        homeBUtton = UIButton.buttonWithType(.System) as? UIButton
        message = UILabel(frame: CGRectZero)
//        view.addSubview(homeBUtton!)
        
//        view.addSubview(message!)
    }

    convenience init() {
        self.init(nibName: nil, bundle: nil)
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableview?.frame = CGRectMake(0, 100, view.bounds.size.width, view.bounds.size.width)
        tableview!.estimatedRowHeight = 80
        
        homeBUtton!.frame = CGRectMake(0, 0, 122, 55)
        homeBUtton!.center = CGPointMake(self.view.bounds.size.width/2, 600)


        message!.frame = CGRectMake(0, 0, 250, 80)
        message!.center = CGPointMake(self.view.bounds.size.width/2, 45)
        
        
        println("\(NSStringFromClass(self.dynamicType)): viewDidLayoutSubviews is called")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        println("hello")
        homeBUtton?.addTarget(self, action: "BacTokHome:", forControlEvents: UIControlEvents.TouchUpInside)
        println("\(NSStringFromClass(self.dynamicType)): viewDidLoad is called")
    }
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = UIColor.greenColor()
        view.addSubview(tableview!)
        homeBUtton!.setTitle("Home", forState: UIControlState.Normal)
        homeBUtton!.backgroundColor = UIColor.yellowColor()
        view.addSubview(homeBUtton!)
        message!.text = "Done!"
        message!.textAlignment = .Center
        message!.backgroundColor = UIColor.orangeColor()
        view.addSubview(message!)
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        println(data.count)
        return numberOfQuestions
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! FinishViewCell
        //        let task = fetchedResultsController.objectAtIndexPath(indexPath) as Tasks
        //        cell.textLabel.text = task.desc
//        cell.textLabel?.text = data[indexPath.row]
        cell.kanji.text = questions[indexPath.row]?.question!
        cell.kanji.sizeToFit()
        cell.yourAns.text = answerSet[indexPath.row].3!
        cell.yourAns.sizeToFit()
        cell.correctAns.text = answerSet[indexPath.row].2
        cell.correctAns.sizeToFit()
        if answerSet[indexPath.row].1 {
            cell.judgeImg.image = UIImage(named: "correct.png")
        } else {
            cell.judgeImg.image = UIImage(named: "incorrect.png")
        }
        cell.judgeImg.sizeToFit()
        cell.frame.size.height = 80
        return cell
    }
    
    
    //  MARK: - UITableVIewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }

    
    func BacTokHome(sender: AnyObject) {
        println("Click BackToHome")
        let svc = KStartViewController(nibName: nil, bundle: nil)
        self.presentViewController(svc, animated: true, completion: nil)
        
    }
    
}
