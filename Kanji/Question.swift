//
//  Question.swift
//  Kanji
//
//  Created by Jumpei Katayama on 1/28/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

class Question: NSObject {
    var question: String?
    var answer: String?
    var choices: [String?]
    
    
    static var numberOfQuestions: Int = 0
//    var answerId: Int
   
    init(question: String?, choices: [String?], answer: String?) {
        self.question = question
        self.choices = choices
        self.answer = answer
        Question.numberOfQuestions += 1
    }
    

}
