//
//  TwiftSearch.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

public extension Twift {
    //	GET		search/tweets
    public func getSearchTweetsWithQuery(q: String, geocode: String? = nil, lang: String? = nil, locale: String? = nil, resultType: String? = nil, count: Int? = nil, until: String? = nil, sinceID: String? = nil, maxID: String? = nil, includeEntities: Bool? = nil, callback: String? = nil, success: ((statuses: [JSON]?, searchMetadata: Dictionary<String, JSON>?) -> Void)? = nil, failure: FailureHandler) {
        let path = "search/tweets.json"
        
        var parameters = Dictionary<String, AnyObject>()
        parameters["q"] = q
        
        if geocode != nil {
            parameters["geocode"] = geocode!
        }
        if lang != nil {
            parameters["lang"] = lang!
        }
        if locale != nil {
            parameters["locale"] = locale!
        }
        if resultType != nil {
            parameters["result_type"] = resultType!
        }
        if count != nil {
            parameters["count"] = count!
        }
        if until != nil {
            parameters["until"] = until!
        }
        if sinceID != nil {
            parameters["since_id"] = sinceID!
        }
        if maxID != nil {
            parameters["max_id"] = maxID!
        }
        if includeEntities != nil {
            parameters["include_entities"] = includeEntities!
        }
        if callback != nil {
            parameters["callback"] = callback!
        }
        
        self.getJSONWithPath(path, baseURL: self.apiURL, parameters: parameters, uploadProgress: nil, downloadProgress: nil, success: {
            json, response in
            println(json)
            switch (json["statuses"].array, json["search_metadata"].object) {
            case (let statuses, let searchMetadata):
                success?(statuses: statuses, searchMetadata: searchMetadata as? [String: JSON])
            default:
                success?(statuses: nil, searchMetadata: nil)
            }
            
            }, failure: failure)
    }
    

    
}