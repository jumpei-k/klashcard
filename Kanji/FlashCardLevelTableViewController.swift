//
//  FlashCardLevelTableViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/13/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//
let menu = ["Beginner", "Intermediate", "Advanced", "Super Advanced"]

import UIKit
import CoreData

class FlashCardLevelTableViewController: UIViewController {

//    var moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.titleView = UIImageView(image: UIImage(named: "navTitle"))
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.078, green: 0.443, blue: 0.941, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        
    }

    override func viewWillAppear(animated: Bool) {
        ///


        if tabBarController?.tabBar.hidden == true {
            tabBarController?.tabBar.hidden = false
//            print("tabbar.hidden = \(tabBarController?.tabBar.hidden)")
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        let requireInitialData = defaults.objectForKey("requireInitialData") as! Bool
        if requireInitialData {
            let loadingDataViewController = storyboard?.instantiateViewControllerWithIdentifier("LoadingDataViewController") as! LoadingDataViewController
            presentViewController(loadingDataViewController, animated: true, completion: nil)
            //            startInitialOperation()
        } else {
            print("unknow error occuered")
        }
    }


    struct SegueIdentifier {
        static let beginer = "beginner units"
        static let intermediate = "intermediate units"
        static let advanced = "advanced units"
        static let superAdvanced = "super advanced units"
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destination = segue.destinationViewController as? FlashCardUnitTableViewController {
            if let identifier = segue.identifier {
                switch identifier {
                case SegueIdentifier.beginer:
                    destination.level = "Beginner"
//                    destination.moc = moc
                case SegueIdentifier.intermediate:
                    destination.level = "Intermediate"
//                    destination.moc = moc
                case SegueIdentifier.advanced:
                    destination.level = "Advanced"
//                    destination.moc = moc
                default: break
                }
            }
        }
        if let dest = segue.destinationViewController as? VocabularyList {
            if let _ = segue.identifier {
                dest.level = "Super Advanced"
//                dest.moc = moc
            }
        }
    }
}

/// return the top UIViewController on navigationController
extension UIViewController {
    var contentViewController: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController!
        } else {
            return self
        }
    }
}
