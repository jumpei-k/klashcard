//
//  GoogleImageViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/26/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit

private let baseURL = "https://www.google.co.jp/search?tbm=isch&q="

class GoogleImageViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var fullURL: String!
    var keyword: String? {
        didSet {
            fullURL = baseURL + keyword!
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "GoogleImage"
        if tabBarController?.tabBar.hidden == false {
            tabBarController?.tabBar.hidden = true
        }
        print("\(NSStringFromClass(self.dynamicType)): viewDidLoad is called")

    }


    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print(self.fullURL)
        if tabBarController?.tabBar.hidden == false {
            tabBarController?.tabBar.hidden = true
            print("tabbar.hidden = \(tabBarController?.tabBar.hidden)")

        }
//        tabBarController?.tabBar.hidden = true
        let set = NSCharacterSet.URLHostAllowedCharacterSet()
        let requURL = NSURL(string: self.fullURL.stringByAddingPercentEncodingWithAllowedCharacters(set)!)!
//        let reqUrl = NSURL(string: self.fullURL.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)

        let request = NSURLRequest(URL: requURL)
        webView?.loadRequest(request)
    }

    override func viewDidAppear(animated: Bool) {
        if tabBarController?.tabBar.hidden == false {
            tabBarController?.tabBar.hidden = true
        }
    }

    override func viewWillDisappear(animated: Bool) {
        if tabBarController?.tabBar.hidden == false {
            tabBarController?.tabBar.hidden = true
        }
    }

    override func viewDidDisappear(animated: Bool) {
        if tabBarController?.tabBar.hidden == false {
            tabBarController?.tabBar.hidden = true
        }
    }
}
