//
//  DataManager.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/7/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

class DataManager {
//    class func getTopAppsDataFromItunesWithSuccess(success: ((iTunesData: NSData!) -> Void)) {
//        //1
//        loadDataFromURL(NSURL(string: TopAppURL)!, completion:{(data, error) -> Void in
//            //2
//            if let urlData = data {
//                //3
//                success(iTunesData: urlData)
//            }
//        })
//    }
    
    class func getKlashCardSetsByLevel(level: String) -> NSData? {
        // pram and the json file name must be the same
        let filePath = NSBundle.mainBundle().pathForResource(level ,ofType:"json")
        do {
            let data = try NSData(contentsOfFile: filePath!, options: NSDataReadingOptions.DataReadingUncached)
            return data
        } catch {
            print(error)
        }
        return nil
    }
    
    class func openJSON(fileName: String) -> NSData? {
//        let filePath = NSBundle.mainBundle().pathForResource(fileName, ofType:"json")
        do {
            let data = try NSData(contentsOfFile: fileName, options: NSDataReadingOptions.DataReadingMappedIfSafe)
            return data
        } catch {
            print(error)
            print("failed to load josn")
        }
        return nil
    }
    
//    success: ((json: JSON) -> Void)
    class func getJSONDataFromFile() -> JSON {
        //1            return JSON(data: data)
        let filePath = NSBundle.mainBundle().pathForResource("allKlashCardData",ofType:"json")
        return JSON(DataManager.openJSON(filePath!)!)
//        success(json: json)
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
//            //2
//
//        })
    }
    
//    NSData?, NSURLResponse?, NSError?
    class func loadDataFromURL(url: NSURL, completion:(data: NSData?, error: NSError?) -> Void) {
        let session = NSURLSession.sharedSession()
        
        _ =  session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
            
            guard let res = response as? NSHTTPURLResponse else { return }
            
            guard res != 200 else {
                let statusError = NSError(domain:"com.raywenderlich", code:res.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                completion(data: nil, error: statusError)
                return
            }
        })

        //                completion(data: nil, error: error)

        
        // Use NSURLSession to get data from an NSURL
//        let loadDataTask = session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
//            guard error == nil else {
//                completion(data: nil, error: error)
//            }
//            
//            if let res = response as? NSHTTPURLResponse {
//                guard res.statusCode != 200 else {
//                    var statusError = NSError(domain:"com.raywenderlich", code:res.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
//                }
//                completion(data: data, error: nil)
//            }
//        })
//        
//        loadDataTask.resume()
    }
    
    // Count the number of kanji json
    //        if let allKlashCardData = json["data"].array {
    //            for kanji in allKlashCardData {
    //
    //                let newKj: KlashCard = KlashCard.createInManagedObjectContext(managedObjectContext, kanji: kanji["kanji"].string!, level: kanji["level"].string!, unit: kanji["unit"].int!, inDock: false, createdDate: NSDate(), yomi_jp: kanji["yomi_jp"].string!, meaning: kanji["meaning"].string!)
    //            }
    //        }
    
    static var calucuratedRows: Int?
    
    let getJSONOperation = NSBlockOperation(block: {
        
    })
    
    /// Returns total kanji data which will be created from a JSON file
    static func totalNumberOfRows(json: JSON) -> Int {
        if let allRows = json["data"].array {
            DataManager.calucuratedRows = allRows.count
            for _ in allRows {
                
            }
        }
        return 0
    }
    
    /// initial load data operation
    /// It executes
    /// 1. Retrieve json data from JSON file
    /// 2. Convert it to Dictionary
    /// 3. Calucurate the number of models
    /// 4. Create a new model and save it. 
    /// 5. Update progress
    /// 6. Repeat 4 and 5
    /// 7. WHen the progress reached 100%, move to the main viewController
    /// 8. When the app goes to background, the operation is suspended.
    /// 9. When the use comes back to the app, resume the operaiton
    /// 10. If error has occuered, the whole operation is cancelled and then alert shows up
    /// 11. The alert asks to retry the operation
    /// This operation should be called only once when the user opened first time.
    ///
    
    static func prepareInitialKlashCardData() {
        _ = NSOperationQueue()
        
    }
}
