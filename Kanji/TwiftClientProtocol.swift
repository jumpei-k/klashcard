//
//  TwiftClientProtocol.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation
import Accounts
import Social


public protocol TwiftClientProtocol {
    
    var credential: TwiftCredential? { get set }
    
    func get(path: String, baseURL: NSURL, parameters: Dictionary<String, AnyObject>, uploadProgress: TwiftHTTPRequest.UploadProgressHandler?, downloadProgress: TwiftHTTPRequest.DownloadProgressHandler?, success: TwiftHTTPRequest.SuccessHandler?, failure: TwiftHTTPRequest.FailureHandler?) -> TwiftHTTPRequest
    
    func post(path: String, baseURL: NSURL, parameters: Dictionary<String, AnyObject>, uploadProgress: TwiftHTTPRequest.UploadProgressHandler?, downloadProgress: TwiftHTTPRequest.DownloadProgressHandler?, success: TwiftHTTPRequest.SuccessHandler?, failure: TwiftHTTPRequest.FailureHandler?) -> TwiftHTTPRequest
    
}