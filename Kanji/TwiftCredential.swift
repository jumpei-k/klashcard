//
//  TwiftCredential.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation
import Accounts


public class TwiftCredential {
    
    public struct OAuthAccessToken {
        
        public internal(set) var key: String
        public internal(set) var secret: String
        public internal(set) var verifier: String?
        
        public internal(set) var screenName: String?
        public internal(set) var userID: String?
        
        public init(key: String, secret: String) {
            self.key = key
            self.secret = secret
        }
        
        public init(queryString: String) {
            var attributes = queryString.parametersFromQueryString()
            
            self.key = attributes["oauth_token"]!
            self.secret = attributes["oauth_token_secret"]!
            
            self.screenName = attributes["screen_name"]
            self.userID = attributes["user_id"]
        }
        
    }
    
    public internal(set) var accessToken: OAuthAccessToken?
    public internal(set) var account: ACAccount?
    
    public init(accessToken: OAuthAccessToken) {
        self.accessToken = accessToken
    }
    
    public init(account: ACAccount) {
        self.account = account
    }

}