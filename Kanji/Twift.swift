//
//  Twift.swift
//  Kanji
//
//  Created by Jumpei Katayama on 5/21/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import Foundation

public class Twift {

    // MARK: - Types
    
    public typealias JSONSuccessHandler = (json: JSON, response: NSHTTPURLResponse) -> Void
    public typealias FailureHandler = (error: NSError) -> Void
    
    struct SwifterError {
        static let domain = "SwifterErrorDomain"
        static let appOnlyAuthenticationErrorCode = 1
    }
    
    // MARK: - Properties
    
    var apiURL: NSURL
    var uploadURL: NSURL
    var streamURL: NSURL
    var userStreamURL: NSURL
    var siteStreamURL: NSURL
    
    var client: TwiftClientProtocol
    
    init(consumerKey: String, consumerSecret: String) {
        self.client = TwiftAppOnlyClient(consumerKey: consumerKey, consumerSecret: consumerSecret)
        
        self.apiURL = NSURL(string: "https://api.twitter.com/1.1/")!
        self.uploadURL = NSURL(string: "https://upload.twitter.com/1.1/")!
        self.streamURL = NSURL(string: "https://stream.twitter.com/1.1/")!
        self.userStreamURL = NSURL(string: "https://userstream.twitter.com/1.1/")!
        self.siteStreamURL = NSURL(string: "https://sitestream.twitter.com/1.1/")!
    }

    // MARK: - JSON Requests
    
    func jsonRequestWithPath(path: String, baseURL: NSURL, method: String, parameters: Dictionary<String, AnyObject>, uploadProgress: TwiftHTTPRequest.UploadProgressHandler? = nil, downloadProgress: JSONSuccessHandler? = nil, success: JSONSuccessHandler? = nil, failure: TwiftHTTPRequest.FailureHandler? = nil) -> TwiftHTTPRequest {
        
        let jsonDownloadProgressHandler: TwiftHTTPRequest.DownloadProgressHandler = {
            data, _, _, response in
            
            if downloadProgress == nil {
                return
            }
            
            var error: NSError?
            let jsonResult = JSON(data: data, options: .MutableContainers, error: &error)
            if jsonResult != nil
            {
                downloadProgress?(json: jsonResult, response: response)
                
            }else{
                let jsonString = NSString(data: data, encoding: NSUTF8StringEncoding)
                let jsonChunks = jsonString!.componentsSeparatedByString("\r\n") as! [String]
                
                for chunk in jsonChunks {
                    if count(chunk.utf16) == 0 {
                        continue
                    }
                    
                    let chunkData = chunk.dataUsingEncoding(NSUTF8StringEncoding)
                    let jsonResult = JSON(data: data, options: .MutableContainers, error: &error)
                    if jsonResult != nil {
                        if let downloadProgress = downloadProgress {
                            downloadProgress(json: jsonResult, response: response)
                        }
                    }
                }
            }
        }
        
        let jsonSuccessHandler: TwiftHTTPRequest.SuccessHandler = {
            data, response in
            
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_UTILITY, 0)) {
                var error: NSError?
                let jsonResult = JSON(data: data, options: .MutableContainers, error: &error)
                if jsonResult != nil {
                    dispatch_async(dispatch_get_main_queue()) {
                        if let success = success {
                            success(json: jsonResult, response: response)
                        }
                    }
                } else {
                    dispatch_async(dispatch_get_main_queue()) {
                        if let failure = failure {
                            failure(error: error!)
                        }
                    }
                }
            }
        }
        
        if method == "GET" {
            return self.client.get(path, baseURL: baseURL, parameters: parameters, uploadProgress: uploadProgress, downloadProgress: jsonDownloadProgressHandler, success: jsonSuccessHandler, failure: failure)
        } else {
            return self.client.post(path, baseURL: baseURL, parameters: parameters, uploadProgress: uploadProgress, downloadProgress: jsonDownloadProgressHandler, success: jsonSuccessHandler, failure: failure)
        }
    }
    
    internal func getJSONWithPath(path: String, baseURL: NSURL, parameters: Dictionary<String, AnyObject>, uploadProgress: TwiftHTTPRequest.UploadProgressHandler?, downloadProgress: JSONSuccessHandler?, success: JSONSuccessHandler?, failure: TwiftHTTPRequest.FailureHandler?) -> TwiftHTTPRequest {
        return self.jsonRequestWithPath(path, baseURL: baseURL, method: "GET", parameters: parameters, uploadProgress: uploadProgress, downloadProgress: downloadProgress, success: success, failure: failure)
    }

}

//extension Twift {
//    func getSearchTweetsWithQuery(q: String, count: Int?, success: ((statuses: [JSON]?, searchMetaData: Dictionary<String, JSON>?) -> Void)? = nil, failure: FailureHandler) {
//        let path = "search/tweet.json"
//        
//        var parameters = Dictionary<String, AnyObject>()
//        parameters["q"] = q
//        
//        if count != nil {
//            parameters["count"] = count!
//        }
//        
//        self.getJSONWithPath(path, baseURL: self.apiURL, parameters: parameters, uploadProgress: nil, downloadProgress: nil, success: {
//            json, response in
//            
//            switch (json["statuses"].array, json["search_metadata"].object) {
//            case (let statuses, let searchMetadata):
//                success?(statuses: statuses, searchMetadata: searchMetadata)
//            default:
//                success?(statuses: nil, searchMetadata: nil)
//            }
//            }, failure: failure)
//    }
//}




