//
//  SearchTableViewController.swift
//  Kanji
//
//  Created by Jumpei Katayama on 6/22/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreData

class SearchTableViewController: UITableViewController {
    let APP_COLOR = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)
    
    var sections: [String]!
    var rows: NSDictionary!
    var values: NSArray!
//    var moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    var searchController: UISearchController? {
        didSet {
            searchController?.searchResultsUpdater = self
            searchController?.delegate = self
            searchController!.dimsBackgroundDuringPresentation = false
            definesPresentationContext = true   // Prevent the search bar from
            searchController!.searchBar.sizeToFit()
            searchController!.searchBar.barTintColor = APP_COLOR
            searchController?.searchBar.tintColor = UIColor.whiteColor()

            tableView.tableHeaderView = searchController!.searchBar
        }
    }
    
    
    var searchResults: [Kanji] = [Kanji]()
    var fetchedResultsController: NSFetchedResultsController?
    
    var noSearch: Bool = true
    
    var searchText: String? {
        didSet {
//            if searchText == "" {
//                sections = ["Twitter", "GoogleImage"]
//                let searchWord = NSArray(object: searchText!)
//                values = NSArray(objects: [""], [""])
//                rows = NSDictionary(objects: values as [AnyObject], forKeys: sections)
//            } else {
//                noSearch = false
//                sections = ["Twitter", "GoogleImage", "This App"]
//                // initialize fetchedResultsController
//                let searchFetchRequest = NSFetchRequest(entityName: "Kanji")
//                let sortDescriptor = NSSortDescriptor(key: "kanji", ascending: true)
//                searchFetchRequest.sortDescriptors = [sortDescriptor]
//                print("search word is \(searchText)")
//                let predicate_kanji = NSPredicate(format: "kanji CONTAINS %@", searchText!)
//                let predicate_yomi = NSPredicate(format: "yomi CONTAINS %@", searchText!)
//                //                let predicate_english = NSPredicate(format: "", <#args: CVarArgType#>...)
////                let compoundPredicate = NSCompoundPredicate.orPredicateWithSubpredicates([predicate_kanji, predicate_yomi])
//                let compoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicate_kanji, predicate_yomi])
//                searchFetchRequest.predicate = compoundPredicate
//                fetchedResultsController = NSFetchedResultsController(fetchRequest: searchFetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
//                // perform fetch
//                do {
//                    try fetchedResultsController!.performFetch()
//                } catch {
//                    print(error)
//                }
//                searchResults = fetchedResultsController!.fetchedObjects as! [Kanji]
//                
//                let searchWord = NSArray(object: searchText!)
//                let values = NSArray(objects: searchWord, searchWord, searchResults)
//                rows = NSDictionary(objects: values as [AnyObject], forKeys: sections)
//            }
        }
    }
    
    // MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.barTintColor = APP_COLOR
        navigationController?.navigationBar.tintColor = APP_COLOR
        
        searchText = ""
        
        title = "Search"
        searchController = UISearchController(searchResultsController: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
//        searchController!.searchBar.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        //        tabBarController?.tabBar.hidden = true
    }
    
    
    struct SegueIdentifier {
//        static let unitTable = "to units table"
        static let twitterFeed = "searchOnTwitter"
        static let searchDetail = "searchDetail"
        static let googleSearch = "googleSearch"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath = tableView.indexPathForCell(sender as! UITableViewCell)
        let textForCell = (sender as! UITableViewCell).textLabel?.text
        if let identifier = segue.identifier {
            switch identifier {
                
            case SegueIdentifier.twitterFeed:
                if let dest = segue.destinationViewController.contentViewController as? TwitterFeedViewController {
                    dest.query = textForCell
                }
                
            case SegueIdentifier.searchDetail:
                if let dest = segue.destinationViewController.contentViewController as? SearchResultDetailViewController {
                    let sectionKey: String = sections[indexPath!.section]
                    let rowsForSection: [Kanji] = rows[sectionKey] as! [Kanji]
                    dest.kanji = rowsForSection[indexPath!.row]
                }
                
            case SegueIdentifier.googleSearch:
                if let dest = segue.destinationViewController.contentViewController as? GoogleImageViewController {
                    print(textForCell)
                    dest.keyword = textForCell
                }
            default: break
            }
        }
    }
    
    
    // ==============================
    // MARK: - Table View Data Source
    // ==============================
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let key = sections[section]
        return rows[key]!.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        let sectionKey: String = sections[indexPath.section]
        let rowsForSection: NSArray = rows[sectionKey] as! NSArray
        if noSearch {
            switch sectionKey {
            case "Twitter":
                cell = tableView.dequeueReusableCellWithIdentifier("TwitterSearchCell", forIndexPath: indexPath)
                
                cell!.textLabel?.text = "'" + (rowsForSection[indexPath.row] as! String) + "'"
                cell?.detailTextLabel?.text = "from Twitter"
                
            case "GoogleImage":
                cell = tableView.dequeueReusableCellWithIdentifier("GoogleImageSearchCell", forIndexPath: indexPath)
//                cell?.imageView?.image = uiima
//                let kanji = self.searchResults[indexPath.row]
                cell!.textLabel?.text = "'" + (rowsForSection[indexPath.row] as! String) + "'"
                cell?.detailTextLabel?.text = "from Google"
                print("hey")
            default: break
            }
        } else {
            switch sectionKey {
            case "Twitter":
                cell = tableView.dequeueReusableCellWithIdentifier("TwitterSearchCell", forIndexPath: indexPath)
                
                cell!.textLabel?.text = "'" + (rowsForSection[indexPath.row] as! String) + "'"
                cell?.detailTextLabel?.text = "from Twitter"
                
            case "GoogleImage":
                cell = tableView.dequeueReusableCellWithIdentifier("GoogleImageSearchCell", forIndexPath: indexPath)
                cell!.textLabel?.text = "'" + (rowsForSection[indexPath.row] as! String) + "'"
                cell?.detailTextLabel?.text = "from Google"
                
            case "This App":
                cell = tableView.dequeueReusableCellWithIdentifier("searchResultCell", forIndexPath: indexPath)
                _ = rowsForSection[indexPath.row] as! Kanji
//                cell!.textLabel?.text = kanji.kanji + " - " + kanji.yomi_jp
            default: break
                
            }
        }

        return cell!

    }
        
}


extension SearchTableViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
}

extension SearchTableViewController: UISearchResultsUpdating {
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        let sb = searchController.searchBar
        searchText = sb.text
        tableView.reloadData()
    }
}

extension SearchTableViewController: UISearchControllerDelegate {
    func willDismissSearchController(searchController: UISearchController) {
        //        print("=================")
        //        print(searchController.active)
    }
    
    func didDismissSearchController(searchController: UISearchController) {
        print("=====dismissing===========")
        print(searchController.active)
        tableView.reloadData()
        print("=====dismised ============")
    }
    
    func willPresentSearchController(searchController: UISearchController) {
        print("////////willPresentingSearchController......///")
        print(searchController.active)
        print("willPresenteeedSearchController")
        
    }
    func didPresentSearchController(searchController: UISearchController) {
        print("didPresenting.....")
        print(searchController.active)
        tableView.reloadData()
        //        tableView.tableHeaderView = searchController.searchBar
        print("didPresented")
    }
    
    func presentSearchController(searchController: UISearchController) {
        // called when search bar is touched
        
        print("presentSearchController")
    }
}

