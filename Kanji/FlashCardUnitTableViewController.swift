
//
//  FlashCardUnitTableViewController.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 5/13/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import CoreData


class FlashCardUnitTableViewController: UITableViewController, UISearchBarDelegate {

    var level: String?
//    var moc: NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = level
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.149, green: 0.517, blue: 0.913, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
//        println("your level is \(self.level!)")
    }
    
    static let segueIdentifier = "to kanji list"
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var destination = segue.destinationViewController
        if let navCon = destination as? UINavigationController {
            destination = navCon.visibleViewController!
        }
        let kanjiLevel = level
        if let destination = segue.destinationViewController as? VocabularyList {
            if segue.identifier == FlashCardUnitTableViewController.segueIdentifier {
                let indexPath = tableView.indexPathForCell(sender as! UITableViewCell)
                destination.unitNumber = indexPath!.row + 1
                destination.level = kanjiLevel
            }
        }
    }

    // ==============================
    // MARK: - Table View Data Source
    // ==============================
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("unit cell", forIndexPath: indexPath) as UITableViewCell
        
        let unitTitle = "Unit" + String(indexPath.row+1)
        cell.textLabel?.text = unitTitle
        return cell
        
    }
}
