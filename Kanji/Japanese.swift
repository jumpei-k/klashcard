//
//  Japanese.swift
//  
//
//  Created by Jumpei Katayama on 5/14/15.
//
//

import Foundation
import CoreData

class Japanese: NSManagedObject {

    @NSManaged var meaning: String
    @NSManaged var kanji: Kanji
//
//    class func createInManagedObjectContext(moc: NSManagedObjectContext, meaning: String) -> Japanese {
//        let newItem = NSEntityDescription.insertNewObjectForEntityForName("Japanese", inManagedObjectContext: moc) as! Japanese
//        newItem.meaning = meaning
//        
//        return newItem
//    }
    
}

