//
//  KlashCardCardView.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 1/7/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import UIKit

class FlashCardHeadsView: UIView {
    
    @IBOutlet weak var kanjiLabel: UILabel!
}

class FlashCardTailsView: UIView {
    @IBOutlet weak var hiraganaLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
}
