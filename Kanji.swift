//
//  Kanji.swift
//  
//
//  Created by Jumpei Katayama on 7/13/15.
//
//

import Foundation
import CoreData

class Kanji: NSManagedObject {

    @NSManaged var createdDate: NSDate
    @NSManaged var inDeck: NSNumber
    @NSManaged var kanji: String
    @NSManaged var level: String
    @NSManaged var unit: NSNumber
    @NSManaged var yomi_jp: String
    @NSManaged var yomi_en: String
    @NSManaged var meaning: String

    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, kanji: String, level: String, unit: NSNumber, yomi: String, inDock: NSNumber = false, createdDate: NSDate? = nil, yomi_en: String = "", yomi_jp: String = "") -> Kanji {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("Kanji", inManagedObjectContext: moc) as! Kanji
        newItem.kanji = kanji
        newItem.level = level
        newItem.unit = unit
        newItem.yomi_jp = yomi_jp
        newItem.yomi_en = yomi_en
        newItem.inDeck = inDock
        newItem.createdDate = NSDate()
        newItem.meaning = meaning
        
        
        return newItem
    }
}
