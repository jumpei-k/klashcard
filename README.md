# KlashCard (Flashcard + Kanji)

Learning Japanese is challenging. It has three types of characters: Kanji, Katakana, Hiragana. Its grammar is totally different from English. It's enough to make you feel overwhelming. Especially, for non-Japanese, Kanji is one of the challenging things. Though there are many apps and text books out there, they might not be that effective in terms of how much you could use kanji as good as native Japanese do.


There are a lot of people who struggling with Japanese Kanji.
KlashCard helps you study Kanji effectively.

## Feature
- Support about 3000 Knanji
- There're four levels: Beginner, Intermediate, Advanced, and SuperAdvanced.
- Sound support for all kanji

## Dependencies
KlashCard uses the dependencies below
- CoreStore 1.6.3
- Fabric 1.6.7
- GCDKit 1.2.2
- SwiftyJSON 2.3.2
- TwitterCore 2.0.2
- TwitterKit 2.0.2

## TODO
- [ ] Add search feature
- [ ] Implement twitter support
