//
//  DataManagerTests.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 2/23/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import XCTest
@testable import KlashCard


class DataManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /// Test the number of items of an array in a json file
    func testLoadJSON() {
        let jsonFilePath = NSBundle.mainBundle().pathForResource("kanji_all_data", ofType:"json")
        let data = DataManager.openJSON(jsonFilePath!)
        let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments) as! NSDictionary
        let items = json["data"] as! [AnyObject]
        XCTAssertEqual(items.count, 3001)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
