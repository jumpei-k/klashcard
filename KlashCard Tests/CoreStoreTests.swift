//
//  CoreStoreTests.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 2/22/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import XCTest
import CoreStore
import Foundation
@testable import KlashCard



class CoreStoreTests: XCTestCase {
    
    private struct Static {
        static let practiceItems: DataStack = {
            let bundle = NSBundle(forClass: CoreStoreTests.self) //NSBundle(forClass:object_getClass(self))
            let dataStack = DataStack(modelName: "TestModel", bundle: bundle)
            
            try! dataStack.addSQLiteStoreAndWait(fileName: "test.sql")
            
            dataStack.beginSynchronous { (transaction) -> Void in
                
                transaction.deleteAll(From(TestKanji))
                

                let kanji1 = transaction.create(Into(TestKanji))
                kanji1.kanji = "食べる"
                kanji1.english = "eat"
                
                let kanji2 = transaction.create(Into(TestKanji))
                kanji2.kanji = "走れ"
                kanji2.english = "run"
                transaction.commit()
            }
            
            return dataStack
        }()
    }
    
    
    let dataStack = KanjiStack.stack
    var sampleData: [TestKanji]?
    override func setUp() {
        super.setUp()
//        lazy var KanjiItems: [Kanji] = {
//            return Static.practiceItems.fetchAll(From(Kanji), OrderBy(.Ascending("kanji")))!
//        }()
        sampleData = Static.practiceItems.fetchAll(From(TestKanji), OrderBy(.Ascending("kanji")))!
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // 
    func testFetch() {
        XCTAssertEqual(sampleData?.count, 2)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
