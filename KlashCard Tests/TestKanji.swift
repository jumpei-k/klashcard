//
//  Kanji+CoreDataProperties.swift
//  
//
//  Created by Jumpei Katayama on 1/10/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData
@testable import KlashCard



class TestKanji: NSManagedObject {

}

extension TestKanji {

    @NSManaged var kanji: String?
    @NSManaged var english: String?
    @NSManaged var hiragana: String?
    @NSManaged var alphabet: String?

}

class City: NSManagedObject {
    
}

extension City {
    
    @NSManaged var name: String?
    
}
