//
//  Kanji_Tests.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 7/13/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

import UIKit
import XCTest
import CoreStore
@testable import KlashCard


class KanjiTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testIncrementalMigration() {
        let dataStack = DataStack(modelName: "Kanji", bundle: NSBundle.mainBundle(), migrationChain: ["Kanji", "Kanji 2", "Kanji 3", "Kanji 4", "Kanji 5", "Kanji 6"])
        CoreStore.defaultStack = dataStack
        XCTAssert(CoreStore.defaultStack === dataStack, "CoreStore.defaultStack === stack")
        
        do {
            
            try dataStack.addSQLiteStoreAndWait(fileName: "kanji.sqlite", configuration: nil, resetStoreOnModelMismatch: true)
        }
        catch let error as NSError {
            
            XCTFail(error.description)
        }


    }

    
    func testExample() {
        let stack = DataStack(modelName: "Kanji", bundle: NSBundle.mainBundle())
        CoreStore.defaultStack = stack
        XCTAssert(CoreStore.defaultStack === stack, "CoreStore.defaultStack === stack")
        
        do {
            
            try stack.addSQLiteStoreAndWait(fileName: "SingleViewCoreData.sqlite", configuration: "Config", resetStoreOnModelMismatch: true)
        }
        catch let error as NSError {
            
            XCTFail(error.description)
        }
        //
        //        do {
        //
        //            try stack.addSQLiteStoreAndWait(fileName: "city.sqlite", configuration: "City", resetStoreOnModelMismatch: true)
        //        }
        //        catch let error as NSError {
        //
        //            XCTFail(error.description)
        //        }
        
        let expectation = self.expectationWithDescription("Create")
        
        CoreStore.beginAsynchronous {
            (transaction) in
            let c = transaction.create(Into(City))
            c.name = "Los Angeles"
            
            let k = transaction.create(Into(TestKanji))
            k.kanji = "空腹"
            
            let count = transaction.queryValue(
                From<TestKanji>(),
                Select<Int>(.Count("testNumber"))
            )
            XCTAssertTrue(count == 0, "count == 0 (actual: \(count))") // counts only objects in store
            
            transaction.commit {
                (result) in
                let kanjiSets = transaction.fetchCount(From(TestKanji))
                XCTAssertEqual(kanjiSets, 3)
            }
            expectation.fulfill()
            
        }
        
        waitForExpectationsWithTimeout(3, handler: nil)
        
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testNewObject() {
        let moc = setUpInMemoryManagedObjectContext()
        //        let entity = NSEntityDescription.entityForName("Kanji", inManagedObjectContext: moc)
        
//        let newItem = NSEntityDescription.insertNewObjectForEntityForName("Kanji", inManagedObjectContext: moc) as! Kanji
//        newItem.kanji = "忍耐"
//        newItem.level = "Advanced"
//        newItem.unit = 1
//        newItem.yomi_jp = "にんたい"
//        newItem.yomi_en = "nintai"
//        newItem.inDeck = true
//        newItem.createdDate = NSDate()
//        newItem.meaning = "patience"
//        XCTAssertEqual(newItem.kanji, "忍耐", "Not matched")
//        XCTAssertEqual(newItem.level, "Advanced", "Not matched")
//        XCTAssertEqual(newItem.unit, 1, "Not matched")
//        XCTAssertEqual(newItem.yomi_jp, "にんたい", "Not matched")
//        XCTAssertEqual(newItem.yomi_en, "nintai", "Not matched")
        
    }
    
    /**
    For Unittest
    
    :refrence: http://www.andrewcbancroft.com/2015/01/13/unit-testing-model-layer-core-data-swift/
    */
    
    func setUpInMemoryManagedObjectContext() -> NSManagedObjectContext {
        let managedObjectModel = NSManagedObjectModel.mergedModelFromBundles([NSBundle.mainBundle()])!
        
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            try persistentStoreCoordinator.addPersistentStoreWithType(NSInMemoryStoreType, configuration: nil, URL: nil, options: nil)
        } catch {
            print(error)
        }
        
        
        let managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        return managedObjectContext
    }

}
