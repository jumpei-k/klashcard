//
//  ModelTests.swift
//  KlashCard
//
//  Created by Jumpei Katayama on 1/10/16.
//  Copyright © 2016 Jumpei Katayama. All rights reserved.
//

import XCTest
import CoreStore
@testable import KlashCard

class ModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
        deleteStores()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        deleteStores()
        super.tearDown()
    }
    
    /// In-Memory with TestModel
    func testCreateInMemoryStack() {
        let stack = DataStack(modelName: "TestModel", bundle: NSBundle(forClass: self.dynamicType))
        CoreStore.defaultStack = stack
        XCTAssert(CoreStore.defaultStack === stack, "CoreStore.defaultStack === stack")
        
        do {
            // creates an in-memory store with entities from the "Config1" configuration in the .xcdatamodeld file
            let persistentStore = try stack.addInMemoryStoreAndWait(configuration: nil) // persistentStore is an NSPersistentStore instance
            print("Successfully created an in-memory store: \(persistentStore)")
        }
        catch {
            print("Failed creating an in-memory store with error: \(error as NSError)")
        }
        
        do {
//            SingleViewCoreData
            let expectation = expectationWithDescription("AddPersistentStore")
            let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
//            return urls[urls.count-1]
            let url = urls[urls.count-1].URLByAppendingPathComponent("kanji.sqlite")
            try stack.addSQLiteStore(
                fileURL: url, // set the target file URL for the sqlite file
                configuration: nil, // use entities from the "Config2" configuration in the .xcdatamodeld file
                resetStoreOnModelMismatch: true,
                completion: { (result) -> Void in
                    switch result {
                    case .Success(let persistentStore):
                        print("Successfully added sqlite store: \(persistentStore)")
                        expectation.fulfill()
                    case .Failure(let error):
                        print("Failed adding sqlite store with error: \(error)")
                    }
                }
            )
        }
        catch {
            print("Failed adding sqlite store with error: \(error as NSError)")
        }
        
        waitForExpectationsWithTimeout(3, handler: nil)
    }

    
    func testCreateKanjiInMemory() {
        let stack = DataStack(modelName: "TestModel", bundle: NSBundle(forClass: self.dynamicType))
        CoreStore.defaultStack = stack
        XCTAssert(CoreStore.defaultStack === stack, "CoreStore.defaultStack === stack")
        
        do {
            let persistentStore = try stack.addInMemoryStoreAndWait(configuration: "Config1") // persistentStore is an NSPersistentStore instance
            print("Successfully created an in-memory store: \(persistentStore)")
        }
        catch {
            print("Failed creating an in-memory store with error: \(error as NSError)")
        }
        
        do {
            //            SingleViewCoreData
            let expectation = expectationWithDescription("AddPersistentStore")
            let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
            //            return urls[urls.count-1]
            let url = urls[urls.count-1].URLByAppendingPathComponent("SingleViewCoreData.sqlite")
            try stack.addSQLiteStore(
                fileURL: url, // set the target file URL for the sqlite file
                configuration: nil, // use entities from the "Config2" configuration in the .xcdatamodeld file
                resetStoreOnModelMismatch: true,
                completion: { (result) -> Void in
                    switch result {
                    case .Success(let persistentStore):
                        print("Successfully added sqlite store: \(persistentStore)")
                        expectation.fulfill()
                    case .Failure(let error):
                        print("Failed adding sqlite store with error: \(error)")
                    }
                }
            )
        }
        catch {
            print("Failed adding sqlite store with error: \(error as NSError)")
        }
        
        waitForExpectationsWithTimeout(3, handler: nil)
        
        let createExpectation = self.expectationWithDescription("Create")
        
        CoreStore.beginAsynchronous {
            (transaction) in
            //            let c = transaction.create(Into(City))
            //            c.name = "Los Angeles"
            
            let k = transaction.create(Into(TestKanji))
            k.kanji = "空腹"
            
            let count = transaction.queryValue(
                From<TestKanji>(),
                Select<Int>(.Count("testNumber"))
            )
            XCTAssertTrue(count == 0, "count == 0 (actual: \(count))") // counts only objects in store
            
            transaction.commit {
                (result) in
                let kanjiSets = transaction.fetchCount(From(TestKanji))
                XCTAssertEqual(kanjiSets, 3)
            }
            createExpectation.fulfill()
            
        }
        
        waitForExpectationsWithTimeout(3, handler: nil)
    }
    
    
    func testExample() {
        let stack = DataStack(modelName: "TestModel", bundle: NSBundle(forClass: self.dynamicType))
        CoreStore.defaultStack = stack
        XCTAssert(CoreStore.defaultStack === stack, "CoreStore.defaultStack === stack")
        
        do {
            
            try stack.addSQLiteStoreAndWait(fileName: "TestKanji.sqlite", configuration: nil, resetStoreOnModelMismatch: true)
        }
        catch let error as NSError {
                    
            XCTFail(error.description)
        }
        
        
//        
//        do {
//            
//            try stack.addSQLiteStoreAndWait(fileName: "city.sqlite", configuration: "City", resetStoreOnModelMismatch: true)
//        }
//        catch let error as NSError {
//            
//            XCTFail(error.description)
//        }

        let expectation = self.expectationWithDescription("Create")
        
        CoreStore.beginAsynchronous {
            (transaction) in
//            let c = transaction.create(Into(City))
//            c.name = "Los Angeles"
            
            let k = transaction.create(Into(TestKanji))
            k.kanji = "空腹"
            
            let count = transaction.queryValue(
                From<TestKanji>(),
                Select<Int>(.Count("testNumber"))
            )
            XCTAssertTrue(count == 0, "count == 0 (actual: \(count))") // counts only objects in store
            
            transaction.commit {
                (result) in
                let kanjiSets = transaction.fetchCount(From(TestKanji))
                XCTAssertEqual(kanjiSets, 3)
            }
            expectation.fulfill()

        }
        
        waitForExpectationsWithTimeout(3, handler: nil)
        
    
    }
    
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    private func deleteStores() {
        
        do {
            
            let fileManager = NSFileManager.defaultManager()
            try fileManager.removeItemAtURL(
                fileManager.URLsForDirectory(.ApplicationSupportDirectory, inDomains: .UserDomainMask).first!
            )
        }
        catch _ { }
    }

}
